package de.uni.kassel.comtec.citypad.core.domain.util

import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.withContext
import retrofit2.Response
import timber.log.Timber

/**
 * This function is used to launch a coroutine with a Deferred Retrofit call.
 * It will handle the loading state and the error state.
 * @param onLoading: This callback will be called when the request is started.
 * @param onSuccess: This callback will be called when the request was successful.
 * @param onError: This callback will be called when the request failed.
 * @param loadingStateFlow: This flow will be used to set the loading state.
 */
suspend fun <T> Deferred<Response<T>>.launch(
    onLoading: (suspend () -> Unit)? = null,
    onSuccess: (suspend ((body: T) -> Unit))? = null,
    onError: (suspend (() -> Unit))? = null,
    loadingStateFlow: MutableStateFlow<Boolean>? = null
) {
    onLoading?.invoke()
    loadingStateFlow?.value = true
    withContext(Dispatchers.IO) {
        try {
            val response = this@launch.await()
            if (response.isSuccessful) {

                val body = response.body()
                if (body == null) {
                    withContext(Dispatchers.Main) {
                        onError?.invoke()
                    }
                } else {
                    withContext(Dispatchers.Main) {
                        onSuccess?.invoke(body)
                    }
                }

            } else {
                Timber.e("FrostAPI Error", response.message())
                withContext(Dispatchers.Main) {
                    onError?.invoke()
                }
            }
        } catch (e: Exception) {
            withContext(Dispatchers.Main) {
                onError?.invoke()
                Timber.e(e)
            }
        } finally {
            loadingStateFlow?.value = false
        }
    }

}