package de.uni.kassel.comtec.citypad.core.presentation.common

import android.content.Intent
import android.view.ViewGroup
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.viewinterop.AndroidView
import androidx.webkit.WebSettingsCompat
import androidx.webkit.WebViewFeature

/**
 * View composable that displays a web view.
 * @param filePath The path to the file that should be displayed in the web view.
 */
@Composable
fun WebViewComposable(filePath: String) {
    LazyColumn(
        modifier = Modifier.fillMaxSize()
    ) {
        item {
            AndroidView(factory = {
                WebView(it).apply {
                    layoutParams = ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT
                    )

                    if (WebViewFeature.isFeatureSupported(WebViewFeature.ALGORITHMIC_DARKENING)) {
                        WebSettingsCompat.setAlgorithmicDarkeningAllowed(
                            this.settings,
                            true
                        )
                    }

                    webViewClient = MyWebViewClient()
                    loadUrl(filePath)
                }
            }, update = {
                it.loadUrl(filePath)
            })
        }
    }
}

class MyWebViewClient: WebViewClient() {
    @Override
    override fun shouldOverrideUrlLoading(view: WebView, webResourceRequest: WebResourceRequest): Boolean {
        val intent = Intent(Intent.ACTION_VIEW, webResourceRequest.url)
        view.context.startActivity(intent)
        return true
    }
}