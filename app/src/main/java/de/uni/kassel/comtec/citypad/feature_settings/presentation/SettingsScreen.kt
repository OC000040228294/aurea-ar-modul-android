package de.uni.kassel.comtec.citypad.feature_settings.presentation

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Switch
import androidx.compose.material3.SwitchColors
import androidx.compose.material3.SwitchDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import de.uni.kassel.comtec.citypad.BuildConfig
import de.uni.kassel.comtec.citypad.R
import de.uni.kassel.comtec.citypad.ui.theme.CityPadTheme

@Composable
fun SettingsScreen(settingsViewModel: SettingsViewModel = hiltViewModel()) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(8.dp)
    ) {

        val switchColors: SwitchColors = SwitchDefaults.colors(
            checkedThumbColor = MaterialTheme.colorScheme.onSecondary,
            checkedTrackColor = MaterialTheme.colorScheme.secondary,
            uncheckedThumbColor = MaterialTheme.colorScheme.onSurface,
            uncheckedTrackColor = MaterialTheme.colorScheme.surface,
        )

        val soundOn = settingsViewModel.soundOn.collectAsState(initial = true)
        val vibrationOn = settingsViewModel.vibrationOn.collectAsState(initial = true)

        Text(text = stringResource(R.string.settings), style = MaterialTheme.typography.headlineLarge)
        Column(modifier = Modifier.padding(top = 16.dp)) {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Column() {
                    Text(text = stringResource(R.string.sound), style = MaterialTheme.typography.titleMedium)
                    Text(
                        text = stringResource(R.string.settings_sound_description),
                        style = MaterialTheme.typography.bodySmall
                    )
                }
                Switch(
                    checked = soundOn.value,
                    onCheckedChange = { settingsViewModel.setSoundOn(!soundOn.value) },
                    colors = switchColors
                )

            }
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Column() {
                    Text(text = stringResource(R.string.vibration), style = MaterialTheme.typography.titleMedium)
                    Text(
                        text = stringResource(R.string.settings_vibration_description),
                        style = MaterialTheme.typography.bodySmall
                    )
                }
                Switch(
                    checked = vibrationOn.value,
                    onCheckedChange = { settingsViewModel.setVibrationOn(!vibrationOn.value) },
                    colors = switchColors
                )

            }
        }

        Spacer(
            modifier = Modifier
                .height(32.dp)
        )

        Text(text = stringResource(R.string.actual_version) + BuildConfig.VERSION_NAME, textAlign = TextAlign.Center, modifier = Modifier.fillMaxWidth())
    }
}

@Composable
@Preview(showBackground = true)
fun PreviewSettings() {
    CityPadTheme {
        SettingsScreen()
    }
}