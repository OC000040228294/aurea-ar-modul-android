package de.uni.kassel.comtec.citypad.feature_ar.presentation.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import de.uni.kassel.comtec.citypad.R
import de.uni.kassel.comtec.citypad.feature_compass.domain.model.CompassRing
import de.uni.kassel.comtec.citypad.ui.theme.CityPadTheme
import kotlin.math.roundToInt

@Composable
@Preview
fun DrawDistanceViewPreview() {
    CityPadTheme {
        DrawDistanceView(2135f)
    }
}

/**
 * Draws the distance component for ar the artefact.
 *
 * @param distance The distance to the artefact.
 */
@Composable
fun DrawDistanceView(distance: Float) {

    val convertedDistance =
        if (distance < CompassRing.NEARBY.range.last) "0-${CompassRing.NEARBY.range.last}m"
        else if (distance < 1000) "${distance.roundToInt()}m" else "${(String.format("%.2f", distance/1000f))}km"

    Box(
        modifier = Modifier
            .clip(RoundedCornerShape(50))
            .background(MaterialTheme.colorScheme.primary)
            .height(IntrinsicSize.Max)
            .width(IntrinsicSize.Max)
    ) {
        Row(
            horizontalArrangement = Arrangement.SpaceAround,
            modifier = Modifier
                .padding(4.dp)
                .fillMaxWidth()
        ) {
            Spacer(modifier = Modifier.width(10.dp))
            Image(
                painter = painterResource(R.drawable.ic_near),
                modifier = Modifier.size(20.dp),
                contentDescription = null,
                colorFilter = ColorFilter.tint(color = Color.White)
            )
            Spacer(modifier = Modifier.width(10.dp))
            Text(text = convertedDistance, color = Color.White)
            Spacer(modifier = Modifier.width(10.dp))
        }
    }
}

@Composable
@Preview
fun DrawTitleRowViewPreview() {
    CityPadTheme {
        DrawTitleRow("Verkehrsaufkommen heute und so weiter", R.drawable.ic_van)
    }
}

/**
 * Draws the title row for the artefact.
 *
 * @param title The title of the artefact.
 * @param iconID The icon of the artefact.
 */
@Composable
fun DrawTitleRow(title: String, iconID: Int) {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .background(MaterialTheme.colorScheme.primary).padding(6.dp),
        contentAlignment = Alignment.CenterStart
    ) {

        Image(
            painter = painterResource(iconID),
            contentDescription = null,
            modifier = Modifier
                .height(36.dp)
                .padding(4.dp)
        )
        Text(
            text = title,
            color = MaterialTheme.colorScheme.onPrimary,
            fontSize = 18.sp,
            overflow = TextOverflow.Ellipsis,
            maxLines = 1,
            textAlign = TextAlign.Center,
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 6.dp, bottom = 6.dp, start = 48.dp, end = 48.dp)
        )
        Spacer(modifier = Modifier.width(6.dp))
    }
}