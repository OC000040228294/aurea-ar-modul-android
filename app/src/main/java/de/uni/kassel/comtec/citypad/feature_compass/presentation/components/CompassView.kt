package de.uni.kassel.comtec.citypad.feature_compass.presentation.components

import androidx.appcompat.content.res.AppCompatResources
import androidx.compose.animation.core.*
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.*
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.*
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.graphics.drawscope.rotate
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.res.imageResource
import androidx.compose.ui.text.*
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.IntSize
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.graphics.drawable.toBitmap
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleOwner
import com.airbnb.lottie.compose.*
import de.uni.kassel.comtec.citypad.R
import de.uni.kassel.comtec.citypad.core.domain.model.ArtefactType
import de.uni.kassel.comtec.citypad.feature_compass.domain.model.CompassRing
import de.uni.kassel.comtec.citypad.feature_compass.presentation.CompassViewModel
import kotlin.math.cos
import kotlin.math.sin

private const val ARTEFACT_SYMBOL_SIZE = 80

/**
 * Draws the compass view.
 */
@Composable
fun CompassView(
    compassViewModel: CompassViewModel = hiltViewModel(),
    lifecycleOwner: LifecycleOwner = LocalLifecycleOwner.current,
) {
    DisposableEffect(lifecycleOwner) {
        compassViewModel.startListening()
        val observer = LifecycleEventObserver { _, event ->
            if (event == Lifecycle.Event.ON_RESUME) {
                compassViewModel.startListening()
            } else if (event == Lifecycle.Event.ON_PAUSE) {
                compassViewModel.stopListening()
            }
        }
        lifecycleOwner.lifecycle.addObserver(observer)
        onDispose {
            compassViewModel.stopListening()
            lifecycleOwner.lifecycle.removeObserver(observer)
        }
    }

    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center,
        modifier = Modifier.fillMaxWidth()
    ) {
        Compass()
    }
}

/**
 * Draws the compass view components.
 */
@Composable
private fun Compass() {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .aspectRatio(1.0f)
            .padding(16.dp)
    ) {
        // Compass
        CompassCanvas()
        CompassRaycast()
        ActiveCompassRingCanvas()
        CompassItemsCanvas()
    }
}

/**
 * Draws the aurea compass items.
 */
@Composable
@OptIn(ExperimentalTextApi::class)
private fun CompassItemsCanvas(
    compassViewModel: CompassViewModel = hiltViewModel(),
) {
    val artefacts = compassViewModel.aureaCompassItems.collectAsState().value
    val nearbyCompassItem = compassViewModel.closebyItem.collectAsState().value

    val clusterIcon = rememberImageBitmap(R.drawable.icon_standort)
    val poolIcon = rememberImageBitmap(R.drawable.ic_hallenbad)
    val parkingIcon = rememberImageBitmap(R.drawable.ic_parken)
    val trafficIcon = rememberImageBitmap(R.drawable.ic_van)
    val treeIcon = rememberImageBitmap(R.drawable.ic_baum)
    val lightIcon = rememberImageBitmap(R.drawable.ic_light)
    val waterlevelIcon = rememberImageBitmap(R.drawable.ic_wasserpegel)
    val co2Icon = rememberImageBitmap(R.drawable.ic_co2)

    val activeRingIconColor = MaterialTheme.colorScheme.secondary
    val passivRingIconColor = MaterialTheme.colorScheme.onSurfaceVariant

    val angle = compassViewModel.angle.collectAsState().value
    val location = compassViewModel.location.collectAsState().value ?: return

    val targetedItem = compassViewModel.targetCompassItem.collectAsState().value
    val lockedItem = compassViewModel.lockedCompassItem.collectAsState().value

    val badgeRadius = 25f
    val textMeasure = rememberTextMeasurer()
    val badgeBackgroundColor = MaterialTheme.colorScheme.primary
    val badgeTextColor = MaterialTheme.colorScheme.onPrimary
    val badgeTextStyle = TextStyle(
        color = badgeTextColor,
        fontSize = 12.sp,
        fontWeight = FontWeight.Bold
    )

    Canvas(modifier = Modifier.fillMaxSize()) {
        val w = size.width
        val h = size.height
        val centerX = (w / 2).toInt()
        val centerY = (h / 2).toInt()

        artefacts.values.forEach {
            it.forEach { item ->
                val angleCorrectedBearing =
                    item.angleCorrectedBearing(angle, item.bearingTo(location))
                val offset = calcOffset(centerX, centerY, angleCorrectedBearing, item.ring)
                val sizeCorrectedOffset = IntOffset(
                    offset.x - ARTEFACT_SYMBOL_SIZE / 2,
                    offset.y - ARTEFACT_SYMBOL_SIZE / 2
                )

                // alpha value in percentage
                val alpha = when (item) {
                    lockedItem -> 1f
                    targetedItem -> 0.5f
                    else -> 0.2f
                }

                val iconTint =
                    if (item.ring == compassViewModel.activeCompassRing.value) activeRingIconColor else passivRingIconColor

                // draw artefact icon
                if (item.artefacts.size == 1) {
                    val icon = when (item.artefacts[0].type) {
                        ArtefactType.TREE -> treeIcon
                        ArtefactType.TRAFFIC -> trafficIcon
                        ArtefactType.PARKING -> parkingIcon
                        ArtefactType.POOL -> poolIcon
                        ArtefactType.LIGHT -> lightIcon
                        ArtefactType.WATERLEVEL -> waterlevelIcon
                        ArtefactType.CO2 -> co2Icon
                    }

                    drawImage(
                        image = icon,
                        dstSize = IntSize(ARTEFACT_SYMBOL_SIZE, ARTEFACT_SYMBOL_SIZE),
                        dstOffset = sizeCorrectedOffset,
                        alpha = alpha,
                        colorFilter = ColorFilter.tint(iconTint)
                    )
                }

                // draw cluster icon with number badge
                else if (item.artefacts.size > 1) {
                    drawImage(
                        image = clusterIcon,
                        dstSize = IntSize(ARTEFACT_SYMBOL_SIZE, ARTEFACT_SYMBOL_SIZE),
                        dstOffset = sizeCorrectedOffset,
                        alpha = alpha,
                        colorFilter = ColorFilter.tint(iconTint)
                    )

                    val badgeOffset = Offset(
                        x = sizeCorrectedOffset.x.toFloat() + ARTEFACT_SYMBOL_SIZE - 20,
                        y = sizeCorrectedOffset.y.toFloat()
                    )

                    drawCircle(
                        color = badgeBackgroundColor.copy(alpha = alpha),
                        center = badgeOffset,
                        radius = badgeRadius,
                    )

                    val clusterNumberText = AnnotatedString((item.artefacts.size).toString())
                    val textLayoutResult: TextLayoutResult =
                        textMeasure.measure(
                            text = clusterNumberText,
                            style = badgeTextStyle,
                        )
                    val textSize = textLayoutResult.size

                    drawText(
                        textMeasurer = textMeasure,
                        text = clusterNumberText,
                        size = Size(badgeRadius * 2, badgeRadius * 2),
                        topLeft = Offset(
                            x = badgeOffset.x - (textSize.width / 2f),
                            y = badgeOffset.y - (textSize.height / 2f)
                        ),
                        style = badgeTextStyle,
                    )
                }
            }
        }
    }

    if (nearbyCompassItem.artefacts.isNotEmpty()) {
        Canvas(modifier = Modifier.fillMaxSize()) {
            val w = size.width
            val h = size.height
            val centerX = (w / 2).toInt()
            val centerY = (h / 2).toInt()

            val sizeCorrectedOffset =
                IntOffset(centerX - ARTEFACT_SYMBOL_SIZE / 2, centerY - ARTEFACT_SYMBOL_SIZE / 2)

            // alpha value in percentage
            val alpha =
                if (compassViewModel.activeCompassRing.value == CompassRing.NEARBY) {
                    1f
                } else {
                    0.2f
                }
            val iconTint =
                if (CompassRing.NEARBY == compassViewModel.activeCompassRing.value) activeRingIconColor else passivRingIconColor

            if (nearbyCompassItem.artefacts.size == 1) {
                val icon = when (nearbyCompassItem.artefacts[0].type) {
                    ArtefactType.TREE -> treeIcon
                    ArtefactType.TRAFFIC -> trafficIcon
                    ArtefactType.PARKING -> parkingIcon
                    ArtefactType.POOL -> poolIcon
                    ArtefactType.LIGHT -> lightIcon
                    ArtefactType.WATERLEVEL -> waterlevelIcon
                    ArtefactType.CO2 -> co2Icon
                }

                drawImage(
                    image = icon,
                    dstSize = IntSize(ARTEFACT_SYMBOL_SIZE, ARTEFACT_SYMBOL_SIZE),
                    dstOffset = sizeCorrectedOffset,
                    alpha = alpha,
                    colorFilter = ColorFilter.tint(iconTint)
                )
            }

            // draw cluster icon with number badge
            else if (nearbyCompassItem.artefacts.size > 1) {

                drawImage(
                    image = clusterIcon,
                    dstSize = IntSize(ARTEFACT_SYMBOL_SIZE, ARTEFACT_SYMBOL_SIZE),
                    dstOffset = sizeCorrectedOffset,
                    alpha = alpha,
                    colorFilter = ColorFilter.tint(iconTint)
                )

                val badgeOffset = Offset(
                    x = sizeCorrectedOffset.x.toFloat() + ARTEFACT_SYMBOL_SIZE - 20,
                    y = sizeCorrectedOffset.y.toFloat()
                )

                drawCircle(
                    color = badgeBackgroundColor.copy(alpha = alpha),
                    center = badgeOffset,
                    radius = badgeRadius,
                )

                val clusterNumberText =
                    AnnotatedString((nearbyCompassItem.artefacts.size).toString())
                val textLayoutResult: TextLayoutResult =
                    textMeasure.measure(
                        text = clusterNumberText,
                        style = badgeTextStyle,
                    )
                val textSize = textLayoutResult.size

                drawText(
                    textMeasurer = textMeasure,
                    text = clusterNumberText,
                    size = Size(badgeRadius * 2, badgeRadius * 2),
                    topLeft = Offset(
                        x = badgeOffset.x - (textSize.width / 2f),
                        y = badgeOffset.y - (textSize.height / 2f)
                    ),
                    style = badgeTextStyle,
                )
            }
        }
    }
}

@Composable
private fun CompassRaycast(compassViewModel: CompassViewModel = hiltViewModel()) {
    val activeCompassRing by compassViewModel.activeCompassRing.collectAsState()
    if (activeCompassRing == CompassRing.NEARBY) return

    val ringFactor by animateFloatAsState(
        activeCompassRing.ringFactor.toFloat()*2+75f,
        animationSpec = spring(
            dampingRatio = Spring.DampingRatioLowBouncy,
            stiffness = Spring.StiffnessMedium
        ), label = "RayCastJumpAnimation"
    )

    val gradientColors = listOf(MaterialTheme.colorScheme.onSurfaceVariant, MaterialTheme.colorScheme.secondary)

    Canvas(modifier = Modifier.fillMaxSize()) {
        val w = size.width
        val h = size.height
        val centerX = (w / 2).toInt()
        val centerY = (h / 2).toInt()

        val outerAngle = 60f
        val innerAngle = 25f

        val width = ringFactor
        val height = ringFactor

        val gradientBrush = Brush.linearGradient(
            colors = gradientColors,
            start = Offset(centerX.toFloat() - 200, centerY.toFloat() - 200),
            end = Offset(centerX.toFloat() + 200, centerY.toFloat() + 200)
        )


        drawArc(
            brush = gradientBrush,
            startAngle = -90f - outerAngle / 2,
            sweepAngle = outerAngle,
            useCenter = true,
            topLeft = Offset(centerX.toFloat() - width / 2, centerY.toFloat() - height / 2),
            size = Size(width, height),
            alpha = 0.15f
        )
        drawArc(
            brush = gradientBrush,
            startAngle = -90f - innerAngle / 2,
            sweepAngle = innerAngle,
            useCenter = true,
            topLeft = Offset(centerX.toFloat() - width / 2, centerY.toFloat() - height / 2),
            size = Size(width, height),
            alpha = 0.3f
        )
    }
}

@Composable
private fun CompassCanvas(
    compassViewModel: CompassViewModel = hiltViewModel()
) {
    val angle = compassViewModel.angle.collectAsState().value
    val lightTheme = !isSystemInDarkTheme()
    val resources = LocalContext.current.resources
    val compassImage =
        if (lightTheme) R.drawable.compassv2 else R.drawable.compassdarkv2
    Canvas(modifier = Modifier.fillMaxSize()) {
        //draw compass image rotated by phone rotation on canvas
        rotate(-angle) {
            drawImage(
                image = ImageBitmap.imageResource(
                    res = resources,
                    id = compassImage
                ),
                dstSize = IntSize(size.width.toInt(), size.height.toInt()),
            )
        }
    }
}

@Composable
private fun ActiveCompassRingCanvas(
    compassViewModel: CompassViewModel = hiltViewModel()
) {
    val activeCompassRing = compassViewModel.activeCompassRing.collectAsState().value

    val ringFactor by animateIntAsState(
        activeCompassRing.ringFactor,
        animationSpec = spring(
            dampingRatio = Spring.DampingRatioLowBouncy,
            stiffness = Spring.StiffnessMedium
        ), label = "RingJumpAnimation"
    )

    val lightTheme = !isSystemInDarkTheme()
    val activeRingColor =
        if (lightTheme) MaterialTheme.colorScheme.secondary else MaterialTheme.colorScheme.secondary
    Canvas(modifier = Modifier.fillMaxSize()) {
        if (activeCompassRing.ordinal == CompassRing.NEARBY.ordinal) {
            drawCircle(
                activeRingColor,
                ringFactor.toFloat(),
                Offset(size.width / 2, size.height / 2),
                style = Stroke(20f),
                alpha = 0.1f
            )
        } else {
            drawCircle(
                activeRingColor,
                ringFactor.toFloat(),
                Offset(size.width / 2, size.height / 2),
                style = Stroke(20f),
                alpha = 0.3f
            )
            drawCircle(
                activeRingColor,
                ringFactor.toFloat(),
                Offset(size.width / 2, size.height / 2),
                style = Stroke(75f),
                alpha = 0.1f
            )
        }
    }
    if (activeCompassRing.ordinal == CompassRing.NEARBY.ordinal) CompassRingNearbyAnimation()
}

@Composable
private fun CompassRingNearbyAnimation() {
    val composition by rememberLottieComposition(LottieCompositionSpec.RawRes(R.raw.nearby))
    val progress by animateLottieCompositionAsState(
        composition,
        iterations = LottieConstants.IterateForever
    )
    Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
        LottieAnimation(
            modifier = Modifier.size(70.dp),
            alignment = Alignment.Center,
            composition = composition,
            progress = { progress },
        )
    }
}

@Composable
private fun rememberImageBitmap(
    drawableRes: Int
): ImageBitmap {
    val context = LocalContext.current
    val artefactIcon = remember {
        AppCompatResources.getDrawable(context, drawableRes)!!.toBitmap().asImageBitmap()
    }
    return artefactIcon
}

private fun calcOffset(
    centerX: Int,
    centerY: Int,
    deg: Float,
    compassRing: CompassRing
): IntOffset {
    val radians = Math.toRadians(deg.toDouble())

    val newX = centerX + sin(radians) * compassRing.ringFactor
    val newY = centerY - cos(radians) * compassRing.ringFactor

    return IntOffset(newX.toInt(), newY.toInt())
}


