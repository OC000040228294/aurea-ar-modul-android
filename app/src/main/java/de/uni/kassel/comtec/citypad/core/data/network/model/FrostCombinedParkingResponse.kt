package de.uni.kassel.comtec.citypad.core.data.network.model

import com.squareup.moshi.Json
import de.uni.kassel.comtec.citypad.core.data.database.model.FrostParkingSensor
import de.uni.kassel.comtec.citypad.framework.core.data.database.model.LatLngAlt
import de.uni.kassel.comtec.citypad.core.domain.model.ArtefactType

data class FrostCombinedParkingResponse(
    @Json(name = "value")
    var observations: List<FrostCombinedParkingObservations>?,
) {
    fun toFrostCombinedParkingSensor(id: Int, name: String, latLngAlt: LatLngAlt): FrostParkingSensor {
        return FrostParkingSensor(
            id = id,
            name = name,
            type = ArtefactType.PARKING,
            latLngAlt = latLngAlt,
            state_left = getCombinedParkingStatus(observations?.get(0)?.observations?.get(0)?.result?.state?: ""),
            state_right = getCombinedParkingStatus(observations?.get(1)?.observations?.get(0)?.result?.state?: "")
        )
    }

    private fun getCombinedParkingStatus(value: String): FrostParkingSensorState {
        return when(value) {
            "vacant" -> FrostParkingSensorState.FREE
            "occupied" -> FrostParkingSensorState.OCCUPIED
            else -> FrostParkingSensorState.OCCUPIED
        }
    }
}

data class FrostCombinedParkingObservations (
    @Json(name = "Observations")
    var observations: List<FrostCombinedParkingObservation>?,
)

data class FrostCombinedParkingObservation(
    var result: FrostCombinedParkingObservationResult?,
)

data class FrostCombinedParkingObservationResult(
    @Json(name = "p_state")
    var state: String,
)