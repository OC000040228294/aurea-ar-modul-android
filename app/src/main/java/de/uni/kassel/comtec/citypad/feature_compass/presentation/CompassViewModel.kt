package de.uni.kassel.comtec.citypad.feature_compass.presentation

import android.location.Location
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import de.uni.kassel.comtec.citypad.core.data.DataRepository
import de.uni.kassel.comtec.citypad.core.domain.model.Artefact
import de.uni.kassel.comtec.citypad.feature_compass.domain.model.AureaCompassItem
import de.uni.kassel.comtec.citypad.feature_compass.domain.model.CompassRing
import de.uni.kassel.comtec.citypad.framework.core.data.SensorRepository
import de.uni.kassel.comtec.citypad.framework.core.domain.util.sense.Sense
import de.uni.kassel.comtec.citypad.framework.core.domain.util.sense.SenseMode
import de.uni.kassel.comtec.citypad.framework.feature_compass.domain.CompassCalculation
import de.uni.kassel.comtec.citypad.framework.feature_compass.domain.model.CompassItemLockStatus
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancelChildren
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

// CLOSEBY_MODE_DISTANCE is the distance at which a sensor is considered as nearby
const val CLOSEBY_MODE_DISTANCE = 3.5
// CLOSEBY_MODE_DISABLE_DISTANCE is the distance at which a nearby sensor is considered as not nearby anymore
const val CLOSEBY_MODE_DISABLE_DISTANCE = 5.5

/**
 * ViewModel for the compass feature.
 * @param dataRepository Repository for the api data.
 * @param sensorRepository Repository for the phones sensor data.
 * @param sense Sense object for the haptic feedback.
 */
@HiltViewModel
class CompassViewModel @Inject constructor(
    private val dataRepository: DataRepository,
    private val sensorRepository: SensorRepository,
    private val sense: Sense
) : ViewModel() {
    private val _senseMode = MutableStateFlow(SenseMode.SOUND_AND_VIBRATION)

    private val _aureaCompassItems = MutableStateFlow(mapOf<CompassRing, List<AureaCompassItem>>())
    val aureaCompassItems: StateFlow<Map<CompassRing, List<AureaCompassItem>>> = _aureaCompassItems

    private val _closebyItem = MutableStateFlow(
        AureaCompassItem(artefacts = mutableListOf(), ring = CompassRing.NEARBY)
    )
    val closebyItem: StateFlow<AureaCompassItem> = _closebyItem

    val angle = sensorRepository.angleFlow

    private val _location = MutableStateFlow<Location?>(null)
    val location: StateFlow<Location?> = _location

    private val _lockedAureaCompassItem = MutableStateFlow<AureaCompassItem?>(null)
    val lockedCompassItem: StateFlow<AureaCompassItem?> = _lockedAureaCompassItem

    private val _targetAureaCompassItem = MutableStateFlow<AureaCompassItem?>(null)
    val targetCompassItem: StateFlow<AureaCompassItem?> = _targetAureaCompassItem

    private val _compassMode = MutableStateFlow(CompassMode.NORMAL)

    private val _activeCompassRing = MutableStateFlow(CompassRing.NEAR)
    val activeCompassRing = _activeCompassRing

    private val calculation = CompassCalculation()

    private val dataScopeJob = SupervisorJob()
    private val dataScope = CoroutineScope(Dispatchers.Main + dataScopeJob)

    private val artefactsMap =
        mutableMapOf<String, Artefact>()

    private var previousTargetItem: AureaCompassItem? = null

    init {
        dataRepository.prefSoundOn.combine(dataRepository.prefVibrationOn) { soundOn, vibrationOn ->
            if (soundOn && vibrationOn) _senseMode.value = SenseMode.SOUND_AND_VIBRATION
            else if (soundOn) _senseMode.value = SenseMode.SOUND
            else if (vibrationOn) _senseMode.value = SenseMode.VIBRATION
            else _senseMode.value = SenseMode.NONE
        }.launchIn(viewModelScope)
    }

    fun setSelectedCompassRing(i: CompassRing) {
        if (i == CompassRing.NEARBY && _closebyItem.value.artefacts.isNotEmpty())
            _activeCompassRing.value = i
        else if (aureaCompassItems.value[i]?.isNotEmpty() == true) {
            _activeCompassRing.value = i
            _targetAureaCompassItem.value = null
            _lockedAureaCompassItem.value = null
        } else sense.compassSwipeRingLocked(_senseMode.value)
    }

    fun increaseSelectedCompassRing() {
        val oldRing = _activeCompassRing.value

        when (_activeCompassRing.value) {
            CompassRing.NEARBY -> {
                if (aureaCompassItems.value[CompassRing.NEAR]?.isNotEmpty() == true) {
                    _activeCompassRing.value = CompassRing.NEAR
                } else if (aureaCompassItems.value[CompassRing.MED]?.isNotEmpty() == true) {
                    _activeCompassRing.value = CompassRing.MED
                } else if (aureaCompassItems.value[CompassRing.FAR]?.isNotEmpty() == true) {
                    _activeCompassRing.value = CompassRing.FAR
                }
            }

            CompassRing.NEAR -> {
                if (aureaCompassItems.value[CompassRing.MED]?.isNotEmpty() == true) {
                    _activeCompassRing.value = CompassRing.MED
                } else if (aureaCompassItems.value[CompassRing.FAR]?.isNotEmpty() == true) {
                    _activeCompassRing.value = CompassRing.FAR
                }
            }

            CompassRing.MED -> {
                if (aureaCompassItems.value[CompassRing.FAR]?.isNotEmpty() == true) {
                    _activeCompassRing.value = CompassRing.FAR
                }
            }

            CompassRing.FAR -> {}
        }

        if (oldRing == _activeCompassRing.value) swipeRingLockedFeedback()
        else {
            _targetAureaCompassItem.value = null
            _lockedAureaCompassItem.value = null
        }
    }

    fun decreaseSelectedCompassRing() {
        val oldRing = _activeCompassRing.value

        when (_activeCompassRing.value) {
            CompassRing.NEARBY -> {}
            CompassRing.NEAR -> {
                if (_closebyItem.value.artefacts.isNotEmpty()) {
                    _activeCompassRing.value = CompassRing.NEARBY
                }
            }

            CompassRing.MED -> {
                if (aureaCompassItems.value[CompassRing.NEAR]?.isNotEmpty() == true) {
                    _activeCompassRing.value = CompassRing.NEAR
                } else if (_closebyItem.value.artefacts.isNotEmpty()) {
                    _activeCompassRing.value = CompassRing.NEARBY
                }
            }

            CompassRing.FAR -> {
                if (aureaCompassItems.value[CompassRing.MED]?.isNotEmpty() == true) {
                    _activeCompassRing.value = CompassRing.MED
                } else if (aureaCompassItems.value[CompassRing.NEAR]?.isNotEmpty() == true) {
                    _activeCompassRing.value = CompassRing.NEAR
                } else if (_closebyItem.value.artefacts.isNotEmpty()) {
                    _activeCompassRing.value = CompassRing.NEARBY
                }
            }
        }

        if (oldRing == _activeCompassRing.value) swipeRingLockedFeedback()
        else {
            _targetAureaCompassItem.value = null
            _lockedAureaCompassItem.value = null
        }
    }

    fun swipeRingLockedFeedback() {
        sense.compassSwipeRingLocked(_senseMode.value)
    }

    fun setCurrentRing() {
        var switchRing = false

        if (_activeCompassRing.value == CompassRing.NEARBY
            && _closebyItem.value.artefacts.isEmpty()
        ) {
            switchRing = true
        } else if (_activeCompassRing.value == CompassRing.NEAR
            && _aureaCompassItems.value[CompassRing.NEAR]?.isEmpty() == true
        ) {
            switchRing = true
        } else if (_activeCompassRing.value == CompassRing.MED
            && _aureaCompassItems.value[CompassRing.MED]?.isEmpty() == true
        ) {
            switchRing = true
        } else if (_activeCompassRing.value == CompassRing.FAR
            && _aureaCompassItems.value[CompassRing.FAR]?.isEmpty() == true
        ) {
            switchRing = true
        }

        if (switchRing) {
            if (_closebyItem.value.artefacts.isNotEmpty()) {
                _activeCompassRing.value = CompassRing.NEARBY
            } else if (_aureaCompassItems.value[CompassRing.NEAR]?.isNotEmpty() == true) {
                _activeCompassRing.value = CompassRing.NEAR
            } else if (_aureaCompassItems.value[CompassRing.MED]?.isNotEmpty() == true) {
                _activeCompassRing.value = CompassRing.MED
            } else if (_aureaCompassItems.value[CompassRing.FAR]?.isNotEmpty() == true) {
                _activeCompassRing.value = CompassRing.FAR
            }
        }
    }

    private fun onSensorDataUpdated() {
        val location = _location.value
        if (location == null) {
            Timber.d("Skipping compass item calculation because position is null")
            return
        }
        val items =
            calculateClusters(
                location,
                artefactsMap.values.toList(),
                _closebyItem.value.artefacts.isNotEmpty()
            )
        _aureaCompassItems.value = items


        val closebyArtefacts =
            artefactsMap.values.toList().filter {
                it.latLngAlt.distanceTo(location) in
                        if (_closebyItem.value.artefacts.isNotEmpty()) {
                            // if we are closeby an artefact, we need to use the disable distance,
                            // because these items are nearby by definition
                            0.0..CLOSEBY_MODE_DISABLE_DISTANCE
                        } else {
                            // if we are not closeby an artefact, use the default trigger distance
                            0.0..CLOSEBY_MODE_DISTANCE
                        }
            }

        _closebyItem.value = AureaCompassItem(
            closebyArtefacts.toMutableList(),
            CompassRing.NEARBY,
        )

        // Determine compass mode
        if (_closebyItem.value.artefacts.isNotEmpty()) {
            _compassMode.value = CompassMode.CLOSE
        } else {
            // There is no close item.
            // Check, if there was before and if we moved far enough away from it
            if (_compassMode.value == CompassMode.CLOSE) {
                // Check if we moved far enough away from the locked compass item
                if ((_lockedAureaCompassItem.value?.distanceTo(location)
                        ?: 0.0) > CLOSEBY_MODE_DISABLE_DISTANCE
                ) {
                    _compassMode.value = CompassMode.NORMAL
                }
            } else {
                _compassMode.value = CompassMode.NORMAL
            }
        }

        setCurrentRing()

        // Lock-in logic based on compass ring
        when (_activeCompassRing.value) {
            CompassRing.FAR, CompassRing.MED, CompassRing.NEAR -> {
                items[_activeCompassRing.value]?.let { aureaCompassItems ->
                    calculateLockStatus(location, angle.value, aureaCompassItems)
                }
            }
            CompassRing.NEARBY -> {
                if (_lockedAureaCompassItem.value == null) sense.compassItemLocked(_senseMode.value)
                _lockedAureaCompassItem.value = _closebyItem.value
                _targetAureaCompassItem.value = null
                previousTargetItem = null
            }
        }
    }

    private fun calculateLockStatus(location: Location, angle: Float, aureaCompassItems: List<AureaCompassItem>) {
        val lockStatus = calculation.compassItemLockInCalculation(
            location,
            angle,
            aureaCompassItems.mapIndexed { index, aureaCompassItem ->
                aureaCompassItem.toCompassItem(index)
            }) ?: return
        handleLockStatus(lockStatus, aureaCompassItems)
    }

    private fun handleLockStatus(
        lockStatus: CompassItemLockStatus,
        aureaCompassItems: List<AureaCompassItem>,
    ) {
        if (lockStatus.lockedItem == null) {
            _lockedAureaCompassItem.value = null
        } else {
            _lockedAureaCompassItem.value = aureaCompassItems[lockStatus.lockedItem!!.id]
        }

        if (lockStatus.targetedItem == null) {
            _targetAureaCompassItem.value = null
        } else {
            _targetAureaCompassItem.value = aureaCompassItems[lockStatus.targetedItem!!.id]
        }

        if (lockStatus.isExited()) {
            sense.compassItemExited(_senseMode.value)
        } else if (lockStatus.isEntered()) {
            sense.compassItemEntered(_senseMode.value)
        } else if (lockStatus.isLocked()) {
            sense.compassItemLocked(_senseMode.value)
        }
    }

    fun calculateClusters(
        location: Location,
        artefacts: List<Artefact>,
        useDisableNearbyDistance: Boolean
    ): Map<CompassRing, List<AureaCompassItem>> {
        val firstRingAureaCompassItems =
            artefacts.filter {
                it.latLngAlt.distanceTo(location) in
                        if (useDisableNearbyDistance) {
                            // if we are closeby an artefact, we need to use the disable distance, because these items are nearby by definition
                            CLOSEBY_MODE_DISABLE_DISTANCE..CompassRing.NEAR.range.last.toDouble()
                        } else {
                            // if we are not closeby an artefact, use the default trigger distance
                            CLOSEBY_MODE_DISTANCE..CompassRing.NEAR.range.last.toDouble()
                        }
            }.map {
                AureaCompassItem(
                    mutableListOf(it),
                    CompassRing.NEAR,
                )
            }
        val secondRingAureaCompassItems =
            artefacts.filter {
                it.latLngAlt.distanceTo(location).toInt() in CompassRing.MED.range
            }.map {
                AureaCompassItem(
                    mutableListOf(it),
                    CompassRing.MED,
                )
            }
        val thirdRingAureaCompassItems =
            artefacts.filter {
                it.latLngAlt.distanceTo(location).toInt() in CompassRing.FAR.range
            }.map {
                AureaCompassItem(
                    mutableListOf(it),
                    CompassRing.FAR,
                )
            }


        val firstRing =
            calculation.clusterCompassItems(location, firstRingAureaCompassItems.mapIndexed { index, item ->
                item.toCompassItem(index)
            }).map { compassItem ->
                val itemArtefacts = compassItem.latLngAltItems.map { latLngAltItem ->
                    artefacts.first {
                        it.id == latLngAltItem.id
                    }
                }
                AureaCompassItem(ring = CompassRing.NEAR, artefacts = itemArtefacts.toMutableList())
            }
        val secondRing =
            calculation.clusterCompassItems(location, secondRingAureaCompassItems.mapIndexed { index, item ->
                item.toCompassItem(index)
            }).map { compassItem ->
                val itemArtefacts = compassItem.latLngAltItems.map { latLngAltItem ->
                    artefacts.first {
                        it.id == latLngAltItem.id
                    }
                }
                AureaCompassItem(ring = CompassRing.MED, artefacts = itemArtefacts.toMutableList())
            }
        val thirdRing =
            calculation.clusterCompassItems(location, thirdRingAureaCompassItems.mapIndexed { index, item ->
                item.toCompassItem(index)
            }).map { compassItem ->
                val itemArtefacts = compassItem.latLngAltItems.map { latLngAltItem ->
                    artefacts.first {
                        it.id == latLngAltItem.id
                    }
                }
                AureaCompassItem(ring = CompassRing.FAR, artefacts = itemArtefacts.toMutableList())
            }

        return mapOf(
            CompassRing.NEAR to firstRing,
            CompassRing.MED to secondRing,
            CompassRing.FAR to thirdRing
        )
    }

    fun startListening() {
        this.dataScope.launch {
            dataRepository.treeSensorFlow.collect {
                it.forEach { sensor ->
                    artefactsMap["tree_${sensor.id}"] = sensor
                    onSensorDataUpdated()
                }
            }
        }

        this.dataScope.launch {
            dataRepository.parkingSensorFlow.collect {
                it.forEach { sensor ->
                    artefactsMap["parking_${sensor.id}"] = sensor
                    onSensorDataUpdated()
                }
            }
        }

        this.dataScope.launch {
            dataRepository.lightSensorFlow.collect {
                it.forEach { sensor ->
                    artefactsMap["light_${sensor.id}"] = sensor
                    onSensorDataUpdated()
                }
            }
        }

        this.dataScope.launch {
            dataRepository.swimmingSensorFlow.collect {
                it.forEach { sensor ->
                    artefactsMap["swimming_${sensor.id}"] = sensor
                    onSensorDataUpdated()
                }
            }
        }

        this.dataScope.launch {
            dataRepository.trafficSensorFlow.collect {
                it.forEach { sensor ->
                    artefactsMap["traffic_${sensor.id}"] = sensor
                    onSensorDataUpdated()
                }
            }
        }

        this.dataScope.launch {
            dataRepository.waterlevelSensorFlow.collect {
                it.forEach { sensor ->
                    artefactsMap["waterlevel_${sensor.id}"] = sensor
                    onSensorDataUpdated()
                }
            }
        }

        this.dataScope.launch {
            dataRepository.co2SensorFlow.collect {
                it.forEach { sensor ->
                    artefactsMap["co2_${sensor.id}"] = sensor
                    onSensorDataUpdated()
                }
            }
        }

        this.dataScope.launch {
            sensorRepository.locationFlow.collect {
                if (it != null) {
                    _location.value = it
                    onSensorDataUpdated()
                }
            }
        }

        this.dataScope.launch {
            sensorRepository.angleFlow.collect {
                onSensorDataUpdated()
            }
        }
    }

    fun stopListening() {
        dataScopeJob.cancelChildren()
    }

    enum class CompassMode {
        NORMAL,
        CLOSE
    }
}

