package de.uni.kassel.comtec.citypad.core.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import de.uni.kassel.comtec.citypad.core.data.database.model.FrostCo2Sensor
import de.uni.kassel.comtec.citypad.core.data.database.model.FrostLightSensor
import de.uni.kassel.comtec.citypad.core.data.database.model.FrostParkingSensor
import de.uni.kassel.comtec.citypad.core.data.database.model.FrostSwimmingPoolSensor
import de.uni.kassel.comtec.citypad.core.data.database.model.FrostTrafficSensor
import de.uni.kassel.comtec.citypad.core.data.database.model.FrostTreeSensor
import de.uni.kassel.comtec.citypad.core.data.database.model.FrostWaterlevelSensor

/**
 * Room Database for the Frost API sensor data
 */
@Database(
    entities = [FrostSwimmingPoolSensor::class, FrostTreeSensor::class,
        FrostTrafficSensor::class, FrostParkingSensor::class, FrostLightSensor::class,
        FrostWaterlevelSensor::class, FrostCo2Sensor::class],
    version = 1
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun frostTreeSensorDao(): FrostTreeSensorDao
    abstract fun frostSwimmingPoolSensorDao(): FrostSwimmingPoolSensorDao
    abstract fun frostTrafficSensorDao(): FrostTrafficSensorDao
    abstract fun frostParkingSensorDao(): FrostParkingSensorDao
    abstract fun frostLightSensorDao(): FrostLightSensorDao
    abstract fun frostWaterlevelSensorDao(): FrostWaterlevelSensorDao
    abstract fun frostCo2SensorDao(): FrostCo2SensorDao
}