package de.uni.kassel.comtec.citypad.core.data.network

import de.uni.kassel.comtec.citypad.core.data.network.model.*
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Route definitions of the KVV Frost API.
 */
interface FrostApi {
    @GET("Datastreams({id})/Observations?\$orderby=resultTime%20desc&\$top=1")
    fun getTreeAsync(@Path("id") id: Int): Deferred<Response<FrostTreeResponse>>

    @GET("Datastreams({id})/Observations?\$orderby=resultTime%20desc&\$top=1")
    fun getTrafficAsync(@Path("id") id: Int): Deferred<Response<FrostTrafficResponse>>

    @GET("Datastreams({id})/Observations?\$orderby=phenomenonTime%20desc&\$top=1")
    fun getSwimmingPoolAsync(@Path("id") id: Int): Deferred<Response<FrostSwimmingPoolResponse>>

    @GET("Datastreams({id})/Observations?\$orderby=phenomenonTime%20desc&\$top=1")
    fun getParkingAsync(@Path("id") id: Int): Deferred<Response<FrostParkingResponse>>

    @GET("Datastreams({id})/Observations?\$orderby=phenomenonTime%20desc&\$top=1")
    fun getWaterlevelAsync(@Path("id") id: Int): Deferred<Response<FrostWaterlevelResponse>>

    @GET("Datastreams({id})/Observations?\$orderby=phenomenonTime%20desc&\$top=1")
    fun getCo2Async(@Path("id") id: Int): Deferred<Response<FrostCo2Response>>

    @GET("Datastreams({id})/Observations?\$orderby=phenomenonTime%20desc&\$top=1")
    fun getLightAsync(@Path("id") id: Int): Deferred<Response<FrostLightResponse>>

    @GET("Datastreams?\$filter=@iot.id%20eq%20244%20or%20@iot.id%20eq%20245&\$expand=Observations(\$orderby=phenomenonTime%20desc;\$top=1)&\$select=Observations")
    fun getCombinedParkingAsync(): Deferred<Response<FrostCombinedParkingResponse>>

    @GET("Datastreams({id})/Thing/Locations")
    fun getLocationAsync(@Path("id") id: Int): Deferred<Response<FrostLocationResponse>>
}