package de.uni.kassel.comtec.citypad.feature_compass.domain.model

import android.location.Location
import de.uni.kassel.comtec.citypad.core.domain.model.Artefact
import de.uni.kassel.comtec.citypad.framework.feature_compass.domain.model.CompassItem
import de.uni.kassel.comtec.citypad.framework.feature_compass.domain.model.LatLngAltItem
import de.uni.kassel.comtec.citypad.feature_compass.presentation.CLOSEBY_MODE_DISABLE_DISTANCE

/**
 * The ranges and radios factors of the compass rings.
 *
 * @param range The range of the ring.
 * @param ringFactor The factor of the ring.
 */
enum class CompassRing(val range: IntRange, val ringFactor: Int) {
    FAR(201..Int.MAX_VALUE, 328),
    MED(51..200, 217),
    NEAR(CLOSEBY_MODE_DISABLE_DISTANCE.toInt()+1..50, 110),
    NEARBY(0..CLOSEBY_MODE_DISABLE_DISTANCE.toInt(), 45),
}

/**
 * The aurea specific compass item that is used for the compass view.
 *
 * @param artefacts The artefacts that are in the compass item.
 * @param ring The ring of the compass item.
 */
data class AureaCompassItem(
    val artefacts: MutableList<Artefact>,
    val ring: CompassRing,
) {
    fun angleCorrectedBearing(currentAngle: Float, bearing: Float): Float {
        return (bearing - currentAngle) % 360
    }

    fun bearingTo(currentLocation: Location): Float {
        val meanBearing =
            artefacts.map { currentLocation.bearingTo(it.latLngAlt.toLocation()) }.average()
        return meanBearing.toFloat()
    }

    fun distanceTo(currentLocation: Location): Double {
        return artefacts.map { currentLocation.distanceTo(it.latLngAlt.toLocation()) }.average()
    }

    fun toCompassItem(id: Int): CompassItem {
        val latLngAltItems = this.artefacts.map {
            LatLngAltItem(it.id, it.latLngAlt)
        }
        return CompassItem(id, latLngAltItems.toMutableList())
    }
}