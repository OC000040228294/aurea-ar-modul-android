package de.uni.kassel.comtec.citypad.core.data.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import de.uni.kassel.comtec.citypad.core.data.database.model.FrostSwimmingPoolSensor
import kotlinx.coroutines.flow.Flow

@Dao
interface FrostSwimmingPoolSensorDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(frostSwimmingPoolSensor: FrostSwimmingPoolSensor)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(frostSwimmingPoolSensors: List<FrostSwimmingPoolSensor>)

    @Query("SELECT * FROM FrostSwimmingPoolSensor")
    fun getAsLiveData(): LiveData<List<FrostSwimmingPoolSensor>>

    @Query("SELECT * FROM FrostSwimmingPoolSensor")
    fun getAsFlow(): Flow<List<FrostSwimmingPoolSensor>>

    @Query("DELETE FROM FrostSwimmingPoolSensor")
    suspend fun delete()
}