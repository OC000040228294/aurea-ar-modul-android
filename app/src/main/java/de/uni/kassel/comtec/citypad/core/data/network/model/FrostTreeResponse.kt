package de.uni.kassel.comtec.citypad.core.data.network.model

import com.squareup.moshi.Json
import de.uni.kassel.comtec.citypad.core.data.database.model.FrostTreeSensor
import de.uni.kassel.comtec.citypad.framework.core.data.database.model.LatLngAlt
import de.uni.kassel.comtec.citypad.core.domain.model.ArtefactType

data class FrostTreeResponse(
    @Json(name = "value")
    var observations: List<FrostTreeObservation>?,
) {
    fun toFrostTreeSensor(id: Int, name: String, latLngAlt: LatLngAlt): FrostTreeSensor {
        return FrostTreeSensor(
            id,
            name,
            latLngAlt,
            ArtefactType.TREE,
            getMoisture(observations?.get(0)?.result?.moisture1?:""),
            getMoisture(observations?.get(0)?.result?.moisture2?:""),
            getMoisture(observations?.get(0)?.result?.moisture3?:""),
        )
    }

    private fun getMoisture(value: String): FrostTreeSensorMoisture {
        return when(value) {
            "low" -> FrostTreeSensorMoisture.LOW
            "medium" -> FrostTreeSensorMoisture.MEDIUM
            "high" -> FrostTreeSensorMoisture.HIGH
            else -> FrostTreeSensorMoisture.UNKNOWN
        }
    }
}

data class FrostTreeObservation(
    var result: FrostTreeObservationResult?,
)

data class FrostTreeObservationResult(
    @Json(name = "soil_moisture_1")
    var moisture1: String,
    @Json(name = "soil_moisture_2")
    var moisture2: String,
    @Json(name = "soil_moisture_3")
    var moisture3: String,
)

enum class FrostTreeSensorMoisture(val value: String) {
    LOW("low"),
    MEDIUM("medium"),
    HIGH("high"),
    UNKNOWN("Unknown")
}

