package de.uni.kassel.comtec.citypad.feature_ar.presentation.components

import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

val SensorCardWidth = 300.dp
val SensorCardHeight = 250.dp

val sensorCardViewModifier: Modifier = Modifier
    .width(SensorCardWidth)
    .height(SensorCardHeight)
    .padding(16.dp)