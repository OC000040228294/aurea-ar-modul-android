package de.uni.kassel.comtec.citypad.core.presentation

import android.location.Location
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import de.uni.kassel.comtec.citypad.core.data.DataRepository
import de.uni.kassel.comtec.citypad.framework.core.data.SensorRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancelChildren
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

/**
 * The is the MainViewModel of the app.
 * It is responsible for fetching the data and managing the state of the app.
 * @param dataRepository The repository for fetching the data.
 * @param sensorRepository The repository for the sensor data.
 * @property onboardingDone The state of the onboarding.
 * @property locationSettingsEnabled The state of the location settings.
 * @property loadingState The state of the loading.
 * @property swapState If the View should be swaped to AR or Compass View.+
 * @property compassCalibration If the compass should be calibrated.
 * @property location The current location.
 * @property rotationAngle The current rotation angle.
 *
 */
@HiltViewModel
class MainViewModel @Inject constructor(
    private val dataRepository: DataRepository,
    private val sensorRepository: SensorRepository,
    ) : ViewModel() {

    private val _onboardingDone = dataRepository.onboardingDone
    val onboardingDone: Flow<Boolean> = _onboardingDone

    private val _locationSettingsEnabled = sensorRepository.locationSettingsEnabled
    val locationSettingsEnabled: StateFlow<Boolean> = _locationSettingsEnabled

    private val _loadingState = dataRepository.loadingState
    val loadingState: StateFlow<Boolean> = _loadingState

    private val _swapState = sensorRepository.swapFlow
    val swapState: StateFlow<Boolean> = _swapState

    private val _compassCalibration = sensorRepository.compassCalibrationFlow
    val compassCalibration: StateFlow<Boolean> = _compassCalibration

    private val _location = sensorRepository.locationFlow
    val location: StateFlow<Location?> = _location.asStateFlow()

    val rotationAngle = sensorRepository.angleFlow

    private val dataScopeJob = SupervisorJob()
    private val dataScope = CoroutineScope(Dispatchers.IO + dataScopeJob)

    fun startFetching() {
        dataScope.launch {
            while (isActive){
                dataRepository.fetchFrostSensors(onError = { Timber.e("Error at fetching sensor data. No Internet?") })
                delay(15000)
            }
        }
    }

    fun stopFetching() {
        dataScopeJob.cancelChildren()
    }

    fun onPermissionGranted() {
        sensorRepository.permissionsGranted()
    }

    fun onPermissionDenied() {
        sensorRepository.permissionDenied()
    }

    fun setOnboardingDone(state: Boolean) {
        viewModelScope.launch {
            dataRepository.setOnboardingDone(state)
        }
    }
}
