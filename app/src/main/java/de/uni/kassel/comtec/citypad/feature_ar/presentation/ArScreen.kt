package de.uni.kassel.comtec.citypad.feature_ar.presentation

import android.location.Location
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.ModalBottomSheet
import androidx.compose.material3.Text
import androidx.compose.material3.rememberModalBottomSheetState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleOwner
import de.uni.kassel.comtec.citypad.R
import de.uni.kassel.comtec.citypad.core.data.database.model.FrostCo2Sensor
import de.uni.kassel.comtec.citypad.core.data.database.model.FrostLightSensor
import de.uni.kassel.comtec.citypad.core.data.database.model.FrostParkingSensor
import de.uni.kassel.comtec.citypad.core.data.database.model.FrostSwimmingPoolSensor
import de.uni.kassel.comtec.citypad.core.data.database.model.FrostTrafficSensor
import de.uni.kassel.comtec.citypad.core.data.database.model.FrostTreeSensor
import de.uni.kassel.comtec.citypad.core.data.database.model.FrostWaterlevelSensor
import de.uni.kassel.comtec.citypad.core.domain.model.Artefact
import de.uni.kassel.comtec.citypad.core.domain.model.ArtefactType
import de.uni.kassel.comtec.citypad.core.presentation.common.WebViewComposable
import de.uni.kassel.comtec.citypad.feature_ar.presentation.components.DialogNoArtifactsSelected
import de.uni.kassel.comtec.citypad.feature_ar.presentation.components.DrawCo2
import de.uni.kassel.comtec.citypad.feature_ar.presentation.components.DrawLight
import de.uni.kassel.comtec.citypad.feature_ar.presentation.components.DrawParking
import de.uni.kassel.comtec.citypad.feature_ar.presentation.components.DrawPool
import de.uni.kassel.comtec.citypad.feature_ar.presentation.components.DrawTraffic
import de.uni.kassel.comtec.citypad.feature_ar.presentation.components.DrawTree
import de.uni.kassel.comtec.citypad.feature_ar.presentation.components.DrawWaterlevel
import de.uni.kassel.comtec.citypad.feature_ar.presentation.components.SensorCardHeight
import de.uni.kassel.comtec.citypad.feature_ar.presentation.components.SensorCardWidth
import de.uni.kassel.comtec.citypad.framework.core.domain.model.NearbyProjection
import de.uni.kassel.comtec.citypad.framework.feature_ar.domain.model.ArItem
import de.uni.kassel.comtec.citypad.framework.feature_ar.presentation.ArOverlayView
import de.uni.kassel.comtec.citypad.framework.feature_ar.presentation.ArViewModel
import kotlinx.coroutines.launch

/**
 * ArView is the main view for the AR feature.
 * It contains the camera preview, the AR overlay and the bottom sheet for sensor use-case informations.
 * @param artefacts List of artefacts to display
 * @param nearbyProjection Projection of the nearby sensors
 * @param shouldSwapToCompassCallback Callback returns true if bottom sheet is not shown and the compass can be displayed
 * @param arViewModel ViewModel for the AR feature
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ArView(
    artefacts: List<Artefact>? = null,
    nearbyProjection: NearbyProjection? = null,
    shouldSwapToCompassCallback: (state: Boolean) -> Unit,
    arViewModel: ArViewModel = hiltViewModel(),
    lifecycleOwner: LifecycleOwner = LocalLifecycleOwner.current,
) {
    DisposableEffect(lifecycleOwner) {
        arViewModel.startListening()
        val observer = LifecycleEventObserver { _, event ->
            if (event == Lifecycle.Event.ON_RESUME) {
                arViewModel.startListening()
            } else if (event == Lifecycle.Event.ON_PAUSE) {
                arViewModel.stopListening()
            }
        }
        lifecycleOwner.lifecycle.addObserver(observer)
        onDispose {
            arViewModel.stopListening()
            lifecycleOwner.lifecycle.removeObserver(observer)
        }
    }

    val userLocation by arViewModel.userLocation.collectAsState()
    var selectedArItemId by remember {
        mutableStateOf(-1)
    }

    var openBottomSheet by rememberSaveable { mutableStateOf(false) }
    var webViewUrl by rememberSaveable { mutableStateOf("") }
    val scope = rememberCoroutineScope()
    val bottomSheetState = rememberModalBottomSheetState(
        skipPartiallyExpanded = true,
    )

    shouldSwapToCompassCallback(!openBottomSheet)

    Box(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight()
    ) {
        CameraPreview()

        if (artefacts.isNullOrEmpty()) {
            DialogNoArtifactsSelected()
        } else {
            ArOverlayView(
                arItems = getComposable(artefacts, userLocation),
                selectedArItemIdCallback = { arItemId -> selectedArItemId = arItemId },
                nearbyProjection = nearbyProjection
            )
        }

        if (!openBottomSheet && selectedArItemId != -1) {
            UseCaseButton(onClickUsecase = {
                openBottomSheet = true
                webViewUrl = "file:///android_asset/${artefacts?.get(selectedArItemId)?.type}.html"})
        }

        if (openBottomSheet) {
            ModalBottomSheet(
                onDismissRequest = {
                    openBottomSheet = false
                },
                sheetState = bottomSheetState,
                tonalElevation = 0.dp,
                        ) {

                Box(
                    modifier = Modifier
                        .fillMaxSize()
                        .background(MaterialTheme.colorScheme.background),
                    contentAlignment = Alignment.BottomCenter
                ) {
                    WebViewComposable(filePath = webViewUrl)

                    Button(onClick = {
                        scope.launch { bottomSheetState.hide() }.invokeOnCompletion {
                            if (!bottomSheetState.isVisible) {
                                openBottomSheet = false
                            }
                        }
                    }) {
                        Text("Schließen")
                    }
                }
            }
        }
    }
}

/**
 * Creates a list of ArItems from a list of artefacts that contain composables for the AR overlay.
 * @param artifacts List of artefacts to display
 * @param userLocation Location of the user
 * @return List of ArItems
 */
fun getComposable(artifacts: List<Artefact>?, userLocation: Location?): List<ArItem>? {
    if (artifacts == null) return null

    val arItems = mutableListOf<ArItem>()

    for (selectedArtefact in artifacts) {
        val distance =
            userLocation?.distanceTo(selectedArtefact.latLngAlt.toLocation())
                ?: 0f

        arItems.add(
            ArItem(
                {
                    when (selectedArtefact.type) {
                        ArtefactType.TREE ->
                            DrawTree(
                                sensor = selectedArtefact as FrostTreeSensor,
                                distance = distance
                            )

                        ArtefactType.TRAFFIC ->
                            DrawTraffic(
                                sensor = selectedArtefact as FrostTrafficSensor,
                                distance = distance
                            )

                        ArtefactType.POOL ->
                            DrawPool(
                                sensor = selectedArtefact as FrostSwimmingPoolSensor,
                                distance = distance
                            )

                        ArtefactType.PARKING ->
                            DrawParking(
                                sensor = selectedArtefact as FrostParkingSensor,
                                distance = distance,
                            )

                        ArtefactType.LIGHT ->
                            DrawLight(
                                sensor = selectedArtefact as FrostLightSensor,
                                distance = distance
                            )

                        ArtefactType.WATERLEVEL ->
                            DrawWaterlevel(
                                sensor = selectedArtefact as FrostWaterlevelSensor,
                                distance = distance
                            )

                        ArtefactType.CO2 ->
                            DrawCo2(
                                sensor = selectedArtefact as FrostCo2Sensor,
                                distance = distance
                            )
                    }
                },
                composableHeight = SensorCardHeight,
                composableWidth = SensorCardWidth,
                latLngAlt = selectedArtefact.latLngAlt
            )
        )
    }
    return arItems
}


/**
 * Button to open the bottom sheet with the use-case information.
 * @param onClickUsecase Callback to open the bottom sheet
 */
@Composable
fun UseCaseButton(onClickUsecase: () -> Unit = {}) {
    Row(
        modifier = Modifier
            .fillMaxSize()
            .padding(bottom = 16.dp),
        horizontalArrangement = Arrangement.Center,
        verticalAlignment = Alignment.Bottom
    ) {
        Button(onClick = onClickUsecase) {
            Image(
                painter = painterResource(
                    id = R.drawable.ic_info
                ),
                colorFilter = ColorFilter.tint(MaterialTheme.colorScheme.onPrimary),
                contentDescription = "Info Icon"
            )
            Spacer(modifier = Modifier.width(8.dp))
            Text(text = stringResource(R.string.more_informations))
        }
    }
}
