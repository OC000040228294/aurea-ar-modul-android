package de.uni.kassel.comtec.citypad.feature_about

import android.content.res.Configuration
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import de.uni.kassel.comtec.citypad.R
import de.uni.kassel.comtec.citypad.ui.theme.CityPadTheme

/**
 * About screen of the app.
 * Shows the smart kassel logo and links to the impressum and datenschutz.
 */
@Composable
fun AboutAurea() {
    val localUriHandler = LocalUriHandler.current
    
    val link_impressum = stringResource(id = R.string.url_impressum)
    val link_datenschutz = stringResource(id = R.string.url_datenschutz)

    LazyColumn(modifier = Modifier
        .fillMaxSize()
        .padding(6.dp)) {
        item {
            Text(text = stringResource(R.string.aboutscreen_title), style = MaterialTheme.typography.headlineLarge, textAlign = TextAlign.Center, modifier = Modifier.fillMaxWidth())

            Column(
                modifier = Modifier
                    .padding(top = 16.dp, start = 8.dp, end = 8.dp)
            ) {
                Text(
                    text = stringResource(R.string.aboutscreen_description),
                    textAlign = TextAlign.Center
                )
            }

            Spacer(modifier = Modifier.height(16.dp))

            Row(modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.Center
            ) {
                Row(
                    modifier = Modifier
                        .background(Color.White),
                ) {
                    Image(
                        painter = painterResource(id = R.drawable.logo_smartkassel),
                        contentDescription = "Logo von SmartKassel",
                        modifier = Modifier
                            .height(100.dp)
                            .padding(2.dp)
                    )
                }
            }

            Spacer(modifier = Modifier.height(16.dp))

            Text(text = stringResource(R.string.more_informations), style = MaterialTheme.typography.headlineLarge, modifier = Modifier.fillMaxWidth(), textAlign = TextAlign.Center)
            Column(
                modifier = Modifier
                    .padding(top = 16.dp, start = 8.dp, end = 8.dp)
                    .fillMaxWidth(),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(
                    text = stringResource(R.string.impressum),
                    style = MaterialTheme.typography.bodyLarge,
                    color = MaterialTheme.colorScheme.secondary,
                    modifier = Modifier.clickable {
                        localUriHandler.openUri(link_impressum)
                    }
                )
                Spacer(modifier = Modifier.height(16.dp))
                Text(
                    text = stringResource(R.string.datenschutz),
                    style = MaterialTheme.typography.bodyLarge,
                    color = MaterialTheme.colorScheme.secondary,
                    modifier = Modifier.clickable {
                        localUriHandler.openUri(link_datenschutz)
                    }
                )
            }
        }
    }
}

@Composable
@Preview(name = "Light Mode")
@Preview(name = "Dark Mode", uiMode = Configuration.UI_MODE_NIGHT_YES)
fun AboutPreview() {
    CityPadTheme {
        AboutAurea()
    }
}