package de.uni.kassel.comtec.citypad.core.data.permissions

/**
 * List of all permissions needed by the app.
 */
val permissionsList = listOf(
    android.Manifest.permission.ACCESS_FINE_LOCATION,
    android.Manifest.permission.CAMERA
)