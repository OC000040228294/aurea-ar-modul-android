package de.uni.kassel.comtec.citypad.core.data.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import de.uni.kassel.comtec.citypad.core.data.database.model.FrostWaterlevelSensor
import kotlinx.coroutines.flow.Flow

@Dao
interface FrostWaterlevelSensorDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(frostWaterlevelSensor: FrostWaterlevelSensor)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(frostWaterlevelSensor: List<FrostWaterlevelSensor>)

    @Query("SELECT * FROM FrostWaterlevelSensor")
    fun getAsLiveData(): LiveData<List<FrostWaterlevelSensor>>

    @Query("SELECT * FROM FrostWaterlevelSensor")
    fun getAsFlow(): Flow<List<FrostWaterlevelSensor>>

    @Query("DELETE FROM FrostWaterlevelSensor")
    suspend fun delete()
}