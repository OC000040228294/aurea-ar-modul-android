package de.uni.kassel.comtec.citypad.core.data.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import de.uni.kassel.comtec.citypad.core.data.database.model.FrostLightSensor
import kotlinx.coroutines.flow.Flow

@Dao
interface FrostLightSensorDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(frostParkingSensor: FrostLightSensor)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(frostLightSensors: List<FrostLightSensor>)

    @Query("SELECT * FROM FrostLightSensor")
    fun getAsLiveData(): LiveData<List<FrostLightSensor>>

    @Query("SELECT * FROM FrostLightSensor")
    fun getAsFlow(): Flow<List<FrostLightSensor>>


    @Query("DELETE FROM FrostLightSensor")
    suspend fun delete()
}