package de.uni.kassel.comtec.citypad.core.data.network.model

import com.squareup.moshi.Json
import de.uni.kassel.comtec.citypad.core.data.database.model.FrostTrafficSensor
import de.uni.kassel.comtec.citypad.framework.core.data.database.model.LatLngAlt
import de.uni.kassel.comtec.citypad.core.domain.model.ArtefactType

data class FrostTrafficResponse(
    @Json(name = "value")
    var observations: List<FrostTrafficObservation>?,
) {
    fun toFrostTrafficSensor(id: Int, name: String, latLngAlt: LatLngAlt): FrostTrafficSensor {
        return FrostTrafficSensor(
            id,
            name,
            ArtefactType.TRAFFIC,
            latLngAlt,
            observations?.get(0)?.result?.hugeSum?.toIntOrNull() ?: 0,
            observations?.get(0)?.result?.largeSum?.toIntOrNull() ?: 0,
            observations?.get(0)?.result?.mediumSum?.toIntOrNull() ?: 0,
            observations?.get(0)?.result?.smallSum?.toIntOrNull() ?: 0,
        )
    }
}

data class FrostTrafficObservation(
    var result: FrostTrafficObservationResult?,
)

data class FrostTrafficObservationResult(
    @Json(name = "huge_sum")
    var hugeSum: String,
    @Json(name = "large_sum")
    var largeSum: String,
    @Json(name = "medium_sum")
    var mediumSum: String,
    @Json(name = "small_sum")
    var smallSum: String,
)