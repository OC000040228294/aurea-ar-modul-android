package de.uni.kassel.comtec.citypad.feature_ar.presentation.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import de.uni.kassel.comtec.citypad.R
import de.uni.kassel.comtec.citypad.core.data.database.model.FrostLightSensor
import de.uni.kassel.comtec.citypad.framework.core.data.database.model.LatLngAlt
import de.uni.kassel.comtec.citypad.core.domain.model.ArtefactType
import de.uni.kassel.comtec.citypad.ui.theme.CityPadTheme

/**
 * A card that displays an artefact.
 *
 * @param artefactName The name of the artefact.
 * @param content The content of the card.
 */
@Composable
fun ArtefactCard(
    artefactName: String,
    content: @Composable () -> Unit
) {
    Box(
        modifier = sensorCardViewModifier,
        contentAlignment = Alignment.BottomCenter
    ) {
        Card(
            modifier = Modifier.fillMaxSize()
                .shadow(
                    elevation = 4.dp,
                    shape = RoundedCornerShape(5.dp),
                    ambientColor = MaterialTheme.colorScheme.onBackground,
                    spotColor = MaterialTheme.colorScheme.onBackground
                ),
            colors = CardDefaults.cardColors(
                containerColor = MaterialTheme.colorScheme.surface,
            ),
        ) {
            Row(
                modifier = Modifier.fillMaxWidth(),
            ) {
                DrawTitleRow(artefactName, R.drawable.ic_light)
            }
            Column(
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier
                    .fillMaxSize()
                    .padding(16.dp)
            ) {
                content()
            }
        }
    }
}

@Composable
@Preview
fun ArtefactCardPreview() {
    CityPadTheme {
        ArtefactCard("") {
            DrawLight(
                FrostLightSensor(
                    id = 9,
                    name = "Laterne",
                    latLngAlt = LatLngAlt(51.311761, 9.474192, 168.35),
                    type = ArtefactType.LIGHT,
                    led_brightness = 30
                ),
                2133f
            )
        }
    }
}