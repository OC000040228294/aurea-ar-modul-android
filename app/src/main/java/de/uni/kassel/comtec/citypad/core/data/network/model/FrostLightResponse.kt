package de.uni.kassel.comtec.citypad.core.data.network.model

import com.squareup.moshi.Json
import de.uni.kassel.comtec.citypad.core.data.database.model.FrostLightSensor
import de.uni.kassel.comtec.citypad.framework.core.data.database.model.LatLngAlt
import de.uni.kassel.comtec.citypad.core.domain.model.ArtefactType

data class FrostLightResponse(
    @Json(name = "value")
    var observations: List<FrostLightObservation>?,
) {
    fun toFrostLightSensor(id: Int, name: String, latLngAlt: LatLngAlt): FrostLightSensor {
        return FrostLightSensor(
            id,
            name,
            ArtefactType.LIGHT,
            latLngAlt,
            observations?.get(0)?.result?.led_brightness?.toInt()?: 0
        )
    }
}

data class FrostLightObservation(
    var result: FrostLightObservationResult?,
)

data class FrostLightObservationResult(
    var led_brightness: String,
)