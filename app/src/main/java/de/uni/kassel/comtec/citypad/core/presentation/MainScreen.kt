package de.uni.kassel.comtec.citypad.core.presentation

import androidx.compose.foundation.layout.*
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import androidx.navigation.NavDestination.Companion.hierarchy
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.rememberMultiplePermissionsState
import de.uni.kassel.comtec.citypad.R
import de.uni.kassel.comtec.citypad.core.data.permissions.permissionsList
import de.uni.kassel.comtec.citypad.core.presentation.common.LocationNotActivatedView
import de.uni.kassel.comtec.citypad.core.presentation.common.PermissionsRejectedView
import de.uni.kassel.comtec.citypad.core.presentation.common.SearchingForLocation
import de.uni.kassel.comtec.citypad.core.presentation.navigation.Screen
import de.uni.kassel.comtec.citypad.feature_about.AboutAurea
import de.uni.kassel.comtec.citypad.feature_ar.presentation.ArView
import de.uni.kassel.comtec.citypad.feature_compass.domain.model.AureaCompassItem
import de.uni.kassel.comtec.citypad.feature_compass.domain.model.CompassRing
import de.uni.kassel.comtec.citypad.feature_compass.presentation.CompassScreen
import de.uni.kassel.comtec.citypad.feature_contact.ContactScreen
import de.uni.kassel.comtec.citypad.feature_settings.presentation.SettingsScreen
import de.uni.kassel.comtec.citypad.framework.core.domain.model.NearbyProjection
import de.uni.kassel.comtec.citypad.ui.theme.CityPadTheme

/**
 * Main entry point of the app.
 * Shows the navigation bar and the current screen.
 */
@Composable
fun CityPad() {
    val navController = rememberNavController()

    Scaffold(
        bottomBar = {
            CityPadNavBar(navController)
        },
    ) { innerPadding ->
        NavHost(
            navController,
            startDestination = Screen.Main.route,
            modifier = Modifier.padding(innerPadding)
        ) {
            composable(Screen.About.route) {
                AboutAurea()
            }
            composable(Screen.Main.route) {
                MainScreen()
            }
            composable(Screen.Settings.route) {
                SettingsScreen()
            }
            composable(Screen.Contact.route) {
                ContactScreen()
            }
        }
    }
}

/**
 * Bottom navigation bar for the app.
 */
@Composable
fun CityPadNavBar(navController: NavController) {
    NavigationBar() {
        val navBackStackEntry by navController.currentBackStackEntryAsState()
        val currentDestination = navBackStackEntry?.destination
        listOf(
            Screen.About,
            Screen.Main,
            Screen.Settings,
            Screen.Contact,
        ).forEach { screen ->
            NavigationBarItem(
                icon = {
                    Icon(
                        painter = painterResource(id = screen.iconResId),
                        contentDescription = null
                    )
                },
                label = {
                    Text(
                        stringResource(screen.titleResId),
                        maxLines = 1,
                        overflow = TextOverflow.Ellipsis)
                    },
                selected = currentDestination?.hierarchy?.any { it.route == screen.route } == true,
                onClick = {
                    navController.navigate(screen.route) {
                        // Pop up to the start destination of the graph to
                        // avoid building up a large stack of destinations
                        // on the back stack as users select items
                        popUpTo(navController.graph.findStartDestination().id) {
                            saveState = true
                        }
                        // Avoid multiple copies of the same destination when
                        // reselecting the same item
                        launchSingleTop = true
                        // Restore state when reselecting a previously selected item
                        restoreState = true
                    }
                }
            )
        }
    }
}

/**
 * Main screen of the app.
 * Shows the AR view or the compass view depending on the current swap state.
 * Also shows the compass calibration dialog if the compass is not calibrated yet.
 * @param mainViewModel the view model to use
 *
 */
@OptIn(ExperimentalPermissionsApi::class)
@Composable
fun MainScreen(mainViewModel: MainViewModel = hiltViewModel()) {
    val permissionState = rememberMultiplePermissionsState(permissions = permissionsList)
    val locationSettingsEnabled by mainViewModel.locationSettingsEnabled.collectAsState()
    val location = mainViewModel.location.collectAsState().value
    val swapState by mainViewModel.swapState.collectAsState()
    val angle by mainViewModel.rotationAngle.collectAsState()

    val compassCalibration by mainViewModel.compassCalibration.collectAsState()
    var calibrationDone by remember {
        mutableStateOf(false)
    }

    var useFakeProjection by remember { mutableStateOf(false) }

    var shouldSwapState by remember {
        mutableStateOf(false)
    }
    var lockedCompassItem by remember {
        mutableStateOf<AureaCompassItem?>(null)
    }

    if (permissionState.allPermissionsGranted) {
        mainViewModel.onPermissionGranted()
    } else {
        mainViewModel.onPermissionDenied()
    }

    if (permissionState.shouldShowRationale ||
        !permissionState.allPermissionsGranted ||
        permissionState.revokedPermissions.isNotEmpty()
    ) {
        PermissionsRejectedView()
    } else if (!locationSettingsEnabled) {
        LocationNotActivatedView()
    } else if (location == null) {
        SearchingForLocation()
    } else {
        if (swapState || !shouldSwapState) {
            val fakeProjection = remember {
                NearbyProjection(location, angle)
            }
            ArView(
                artefacts = lockedCompassItem?.artefacts,
                nearbyProjection = if (useFakeProjection) fakeProjection else null,
                shouldSwapToCompassCallback = { state ->
                    shouldSwapState = state
                }
            )
        } else {
            CompassScreen(
                lockedCompassItemCallback = { items ->
                    lockedCompassItem = items
                },
                compassRingCallback = {
                    useFakeProjection = it == CompassRing.NEARBY
                }
            )
        }

        if (compassCalibration && !calibrationDone) {
            CompassCalibrationDialog(callback = {
                calibrationDone = true
            })
        }
    }

}

/**
 * Dialog shown when the compass seems not to be calibrated yet.
 * @param callback callback to call when the user clicks the OK button
 */
@Composable
fun CompassCalibrationDialog(
    callback: () -> Unit,
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(32.dp),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Card(
            elevation = CardDefaults.cardElevation(defaultElevation = 6.dp),
        ) {
            Column(
                modifier = Modifier.padding(16.dp),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(
                    modifier = Modifier.padding(4.dp),
                    text = stringResource(R.string.dialog_compass_calibration_title),
                    style = MaterialTheme.typography.titleLarge
                )
                Text(
                    modifier = Modifier.padding(4.dp),
                    text = stringResource(R.string.dialog_compass_calibration_description),
                    style = MaterialTheme.typography.bodyMedium,
                )

                TextButton(
                    modifier = Modifier.padding(4.dp),
                    onClick = callback)
                {
                    Text(
                        text = stringResource(R.string.ok),
                        color = MaterialTheme.colorScheme.onSecondary)
                }
            }
        }
    }
}

@Composable
@Preview(
    uiMode = android.content.res.Configuration.UI_MODE_NIGHT_NO,
    showBackground = true)
fun CompassCalibrationDialogPreview() {
    CityPadTheme {
        CompassCalibrationDialog(callback = { })
    }
}



