package de.uni.kassel.comtec.citypad.feature_ar.presentation.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import de.uni.kassel.comtec.citypad.R
import de.uni.kassel.comtec.citypad.core.data.database.model.FrostTrafficSensor
import de.uni.kassel.comtec.citypad.framework.core.data.database.model.LatLngAlt
import de.uni.kassel.comtec.citypad.core.domain.model.ArtefactType
import de.uni.kassel.comtec.citypad.ui.theme.CityPadTheme

@Composable
@Preview
fun DrawTrafficPreview() {
    CityPadTheme {
        DrawTraffic(
            FrostTrafficSensor(
                id = 8,
                name = "Verkehr",
                latLngAlt = LatLngAlt(51.312188, 9.472786, 168.35),
                type = ArtefactType.TRAFFIC,
                hugeSum = 20,
                largeSum = 50,
                mediumSum = 1000,
                smallSum = 30
            ),
            2000f)
    }
}

@Composable
fun DrawTraffic(
    sensor: FrostTrafficSensor, distance: Float
) {
    Box(
        modifier = sensorCardViewModifier,
        contentAlignment = Alignment.BottomCenter
    ) {
        Card(
            modifier = Modifier.fillMaxSize()
                .shadow(
                    elevation = 4.dp,
                    shape = RoundedCornerShape(16.dp),
                    ambientColor = MaterialTheme.colorScheme.onBackground,
                    spotColor = MaterialTheme.colorScheme.onBackground
                ),
            colors = CardDefaults.cardColors(
                containerColor = MaterialTheme.colorScheme.surface,
            ),
        ) {
            Row(
                modifier = Modifier.fillMaxWidth(),
            ) {
                DrawTitleRow(sensor.name, R.drawable.ic_van)
            }
            Column(
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier
                    .fillMaxSize()
                    .padding(bottom = 16.dp)
            ) {
                Text("Heute", style = MaterialTheme.typography.titleLarge, modifier = Modifier.padding(bottom = 16.dp))
                Row(
                    horizontalArrangement = Arrangement.Center,
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    for (i in 1..4) {
                        when (i) {
                            1 -> {
                                DrawTrafficRow(sensor.hugeSum, R.drawable.ic_transpoter)
                            }
                            2 -> {
                                DrawTrafficRow(sensor.largeSum, R.drawable.ic_van)
                            }
                            3 -> {
                                DrawTrafficRow(sensor.mediumSum, R.drawable.ic_kleinwagen)
                            }
                            4 -> {
                                DrawTrafficRow(sensor.smallSum, R.drawable.ic_motorrad)
                            }
                        }
                    }
                }
            }
        }
        Box(
            modifier = Modifier.offset(y = (16).dp),
        ) {
            DrawDistanceView(distance)
        }
    }
}

@Composable
fun DrawTrafficRow(sum: Int?, ic: Int) {
    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.padding(horizontal = 4.dp)
    ) {
        Image(
            painter = painterResource(ic),
            modifier = Modifier.size(35.dp),
            contentDescription = null,
        )
        Text(text = String.format("%,d", sum))
    }
}


