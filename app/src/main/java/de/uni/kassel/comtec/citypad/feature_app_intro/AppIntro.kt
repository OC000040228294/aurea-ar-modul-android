package de.uni.kassel.comtec.citypad.feature_app_intro

import android.content.res.Configuration
import androidx.annotation.DrawableRes
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPager
import com.google.accompanist.pager.HorizontalPagerIndicator
import com.google.accompanist.pager.rememberPagerState
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.rememberMultiplePermissionsState
import de.uni.kassel.comtec.citypad.R
import de.uni.kassel.comtec.citypad.core.data.permissions.permissionsList
import de.uni.kassel.comtec.citypad.ui.theme.CityPadTheme
import kotlinx.coroutines.*

/**
 * Data class for the pages in the intro.
 */
data class Page(
    val title: Int,
    val description: Int,
    @DrawableRes val image: Int?
)

/**
 * List of pages to show in the intro.
 */
val onboardPages = listOf(
    Page(
        title = R.string.intro_page1_title,
        description = R.string.intro_page1_description,
        image = R.drawable.intro_compass
    ),
    Page(
        title = R.string.intro_page2_title,
        description = R.string.intro_page2_description,
        image = R.drawable.intro_ar_view,
    ),
    Page(
        title =R.string.intro_page3_title,
        description = R.string.intro_page3_description,
        image = null
    )
)

/**
 * UI for a single page in the intro.
 */
@Composable
fun PageUI(page: Page) {
    Column(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.SpaceBetween
    ) {
        Text(
            text = stringResource(id = page.title),
            style = MaterialTheme.typography.titleLarge,
            color = MaterialTheme.colorScheme.onBackground,
            textAlign = TextAlign.Center,
            modifier = Modifier.padding(16.dp)
        )
        if (page.image != null) {
            Image(
                painter = painterResource(page.image),
                contentDescription = null,
                modifier = Modifier.height(intrinsicSize = IntrinsicSize.Max)
            )
        }
        Text(
            text = stringResource(id = page.description),
            textAlign = TextAlign.Center,
            style = MaterialTheme.typography.bodyLarge,
            color = MaterialTheme.colorScheme.onBackground,
            modifier = Modifier.padding(16.dp)
        )

    }
}

/**
 * Intro screen shown on first app start.
 * @param onGettingStartedClick callback to call when the user clicks the "Getting Started" button
 */
@OptIn(ExperimentalPermissionsApi::class)
@ExperimentalAnimationApi
@ExperimentalPagerApi
@Composable
fun AppIntro(onGettingStartedClick: () -> Unit) {
    val pagerState = rememberPagerState()

    // Camera permission state
    val permissionsState = rememberMultiplePermissionsState(
        permissions = permissionsList
    )

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight()
            .background(MaterialTheme.colorScheme.background)
            .padding(8.dp)
    ) {

        HorizontalPager(
            state = pagerState,
            modifier = Modifier
                .fillMaxWidth()
                .weight(1f),
            count = onboardPages.size,
            userScrollEnabled = false
        ) { page ->
            PageUI(page = onboardPages[page])
        }
        HorizontalPagerIndicator(
            pagerState = pagerState,
            modifier = Modifier
                .align(Alignment.CenterHorizontally)
                .padding(16.dp),
            activeColor = MaterialTheme.colorScheme.onBackground
        )
        val scope = rememberCoroutineScope()
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .align(Alignment.CenterHorizontally),
            horizontalArrangement = Arrangement.End
        ) {
            if (pagerState.currentPage <= 1) {
                Button(
                    onClick = {
                        scope.launch {
                            pagerState.scrollToPage(pagerState.currentPage + 1)
                        }
                    },
                ) {
                    Text(text = stringResource(R.string.intro_button_next))
                }
            } else if (pagerState.currentPage == 2) {
                Button(
                    onClick = {
                        if (!permissionsState.allPermissionsGranted && !permissionsState.shouldShowRationale)
                            permissionsState.launchMultiplePermissionRequest()
                        else {
                            scope.launch {
                                onGettingStartedClick()
                            }
                        }
                    }
                ) {
                    Text(
                        text = if (!permissionsState.allPermissionsGranted && !permissionsState.shouldShowRationale)
                            stringResource(R.string.intro_button_permissions) else stringResource(R.string.intro_button_finish)
                    )
                }
            }
        }
    }
}

@OptIn(ExperimentalAnimationApi::class, ExperimentalPagerApi::class)
@Composable
@Preview
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES)
fun PreviewAppIntro() {
    CityPadTheme {
        AppIntro(onGettingStartedClick = {})
    }
}
