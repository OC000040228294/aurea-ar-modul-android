package de.uni.kassel.comtec.citypad.feature_ar.presentation.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import de.uni.kassel.comtec.citypad.R
import de.uni.kassel.comtec.citypad.core.data.database.model.FrostParkingSensor
import de.uni.kassel.comtec.citypad.framework.core.data.database.model.LatLngAlt
import de.uni.kassel.comtec.citypad.core.data.network.model.FrostParkingSensorState
import de.uni.kassel.comtec.citypad.core.domain.model.ArtefactType
import de.uni.kassel.comtec.citypad.ui.theme.CityPadTheme
import de.uni.kassel.comtec.citypad.ui.theme.ParkingGreen
import de.uni.kassel.comtec.citypad.ui.theme.ParkingRed


@Composable
@Preview
fun DrawParkingPreview() {
    CityPadTheme {
        DrawParking(
            FrostParkingSensor(
                id = 5,
                name = "Parkplätze - Williallee",
                latLngAlt = LatLngAlt(51.312057, 9.473029, 168.35),
                type = ArtefactType.PARKING,
                state_left = FrostParkingSensorState.OCCUPIED,
                state_right = FrostParkingSensorState.FREE
            ), 2133f
        )
    }
}

@Composable
fun DrawParking(
    sensor: FrostParkingSensor,
    distance: Float
) {
    Box(
        modifier = sensorCardViewModifier,
        contentAlignment = Alignment.BottomCenter
    ) {
        Card(
            modifier = Modifier
                .fillMaxSize()
                .shadow(
                    elevation = 4.dp,
                    shape = RoundedCornerShape(16.dp),
                    ambientColor = MaterialTheme.colorScheme.onBackground,
                    spotColor = MaterialTheme.colorScheme.onBackground
                ),
            colors = CardDefaults.cardColors(
                containerColor = MaterialTheme.colorScheme.surface,
            ),
        ) {
            Row(
                modifier = Modifier.fillMaxWidth(),
            ) {
                DrawTitleRow(sensor.name, R.drawable.ic_parken)
            }
            Column(
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier
                    .fillMaxSize()
                    .padding(bottom = 16.dp)
            ) {
                Row(
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically
                ) {

                    Column(
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        var parkingColor: Color = ParkingRed
                        if (sensor.state_left == FrostParkingSensorState.FREE)
                            parkingColor = ParkingGreen

                        Box(
                            contentAlignment = Alignment.Center
                        ) {
                            ParkingPlaceShape(
                                strokeWidth = 10f,
                                color = MaterialTheme.colorScheme.onSurface,
                                modifier = Modifier
                                    .width(70.dp)
                                    .height(100.dp)
                            )
                            Column(
                                horizontalAlignment = Alignment.CenterHorizontally
                            ) {
                                Image(
                                    painter = painterResource(R.drawable.ic_parken),
                                    modifier = Modifier.size(40.dp).padding(8.dp),
                                    contentDescription = null,
                                    colorFilter = ColorFilter.tint(parkingColor)
                                )
                                Spacer(modifier = Modifier.width(8.dp))
                                Text(
                                    text = if (sensor.state_left == FrostParkingSensorState.FREE) "Frei" else "Belegt",
                                    style = MaterialTheme.typography.titleSmall
                                )
                            }
                        }
                    }
                    Spacer(modifier = Modifier.width(8.dp))
                    Column(
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {var parkingColor: Color = ParkingRed
                        if (sensor.state_right == FrostParkingSensorState.FREE)
                            parkingColor = ParkingGreen

                        Box(
                            contentAlignment = Alignment.Center
                        ) {
                            ParkingPlaceShape(
                                strokeWidth = 10f,
                                color = MaterialTheme.colorScheme.onSurface,
                                modifier = Modifier
                                    .width(70.dp)
                                    .height(100.dp)
                            )
                            Column(
                                horizontalAlignment = Alignment.CenterHorizontally
                            ) {

                                Image(
                                    painter = painterResource(R.drawable.ic_parken),
                                    modifier = Modifier.size(40.dp).padding(8.dp),
                                    contentDescription = null,
                                    colorFilter = ColorFilter.tint(parkingColor)
                                )
                                Spacer(modifier = Modifier.width(8.dp))
                                Text(
                                    text = if (sensor.state_right == FrostParkingSensorState.FREE) "Frei" else "Belegt",
                                    style = MaterialTheme.typography.titleSmall
                                )
                            }
                        }
                    }



                }

            }
        }
        Box(
            modifier = Modifier.offset(y = (16).dp),
        ) {
            DrawDistanceView(distance)
        }
    }
}