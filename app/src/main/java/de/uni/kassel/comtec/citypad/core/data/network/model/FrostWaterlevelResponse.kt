package de.uni.kassel.comtec.citypad.core.data.network.model

import com.squareup.moshi.Json
import de.uni.kassel.comtec.citypad.core.data.database.model.FrostWaterlevelSensor
import de.uni.kassel.comtec.citypad.framework.core.data.database.model.LatLngAlt
import de.uni.kassel.comtec.citypad.core.domain.model.ArtefactType
import timber.log.Timber

data class FrostWaterlevelResponse(
    @Json(name = "value")
    var observations: List<FrostWaterlevelObservation>?,
) {
    fun toFrostWaterlevelSensor(id: Int, name: String, latLngAlt: LatLngAlt): FrostWaterlevelSensor {

        var waterlevel = 0
        try {
            waterlevel = observations?.get(0)?.result?.waterlevel?.toInt()?: 0
        } catch (nfe: NumberFormatException) {
            Timber.e("Could not parse waterlevel to int: ${nfe.message}")
        }

        return FrostWaterlevelSensor(
            id,
            name,
            ArtefactType.WATERLEVEL,
            latLngAlt,
            waterlevel
        )
    }
}

data class FrostWaterlevelObservation(
    var result: FrostWaterlevelObservationResult?,
)

data class FrostWaterlevelObservationResult(
    @Json(name = "stand")
    var waterlevel: String,
)