package de.uni.kassel.comtec.citypad.core.data.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import de.uni.kassel.comtec.citypad.core.data.database.model.FrostCo2Sensor
import kotlinx.coroutines.flow.Flow

@Dao
interface FrostCo2SensorDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(frostCo2Sensor: FrostCo2Sensor)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(frostCo2Sensor: List<FrostCo2Sensor>)

    @Query("SELECT * FROM FrostCo2Sensor")
    fun getAsLiveData(): LiveData<List<FrostCo2Sensor>>

    @Query("SELECT * FROM FrostCo2Sensor")
    fun getAsFlow(): Flow<List<FrostCo2Sensor>>

    @Query("DELETE FROM FrostCo2Sensor")
    suspend fun delete()
}