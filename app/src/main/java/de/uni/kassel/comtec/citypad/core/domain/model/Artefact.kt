package de.uni.kassel.comtec.citypad.core.domain.model

import de.uni.kassel.comtec.citypad.R
import de.uni.kassel.comtec.citypad.core.domain.model.ArtefactType.CO2
import de.uni.kassel.comtec.citypad.core.domain.model.ArtefactType.LIGHT
import de.uni.kassel.comtec.citypad.core.domain.model.ArtefactType.PARKING
import de.uni.kassel.comtec.citypad.core.domain.model.ArtefactType.POOL
import de.uni.kassel.comtec.citypad.core.domain.model.ArtefactType.TRAFFIC
import de.uni.kassel.comtec.citypad.core.domain.model.ArtefactType.TREE
import de.uni.kassel.comtec.citypad.core.domain.model.ArtefactType.WATERLEVEL
import de.uni.kassel.comtec.citypad.framework.core.data.database.model.LatLngAlt

/**
 * Types of artefacts that are supported.
 */
enum class ArtefactType(val value: String) {
    TREE("Pflanze"),
    TRAFFIC("Verkehr"),
    POOL("Schwimmbad"),
    PARKING("Parken"),
    LIGHT("Licht"),
    WATERLEVEL("Pegelstand"),
    CO2("Co2");
}

/**
 * Base class for all artefacts.
 */
abstract class Artefact {
    abstract val name: String
    abstract val id: Int
    abstract val latLngAlt: LatLngAlt
    abstract val type: ArtefactType
}

/**
 * Definition of Icons per ArtefactType.
 */
fun Artefact.getDrawableId(): Int{
    return when(this.type) {
        LIGHT -> R.drawable.ic_light
        TREE -> R.drawable.ic_baum
        TRAFFIC -> R.drawable.ic_van
        POOL -> R.drawable.ic_hallenbad
        PARKING -> R.drawable.ic_parken
        WATERLEVEL -> R.drawable.ic_wasserpegel
        CO2 -> R.drawable.ic_co2
    }
}

