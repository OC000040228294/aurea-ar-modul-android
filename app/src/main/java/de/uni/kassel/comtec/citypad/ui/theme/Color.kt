package de.uni.kassel.comtec.citypad.ui.theme

import androidx.compose.ui.graphics.Color


val CitypadBlue = Color(0xFF009fe3)
val CitypadDarkBlue = Color(0xFF242D4B)
val CitypadBlueGrey = Color(0xFFBCD9EE)
val CitypadBlack = Color(0xFF000000)
val CitypadGrey = Color(0xFF5E5E5E)
val White = Color(0xFFFFFFFF)
val BlackBlue = Color(0xFF14161D)
val ParkingRed = Color(0xFDB62B2B)
val ParkingGreen = Color(0xFD94B30E)

