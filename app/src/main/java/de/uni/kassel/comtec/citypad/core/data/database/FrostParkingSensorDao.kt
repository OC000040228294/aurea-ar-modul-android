package de.uni.kassel.comtec.citypad.core.data.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import de.uni.kassel.comtec.citypad.core.data.database.model.FrostParkingSensor
import kotlinx.coroutines.flow.Flow

@Dao
interface FrostParkingSensorDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(frostParkingSensor: FrostParkingSensor)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(frostParkingSensors: List<FrostParkingSensor>)

    @Query("SELECT * FROM FrostParkingSensor")
    fun getAsLiveData(): LiveData<List<FrostParkingSensor>>

    @Query("SELECT * FROM FrostParkingSensor")
    fun getAsFlow(): Flow<List<FrostParkingSensor>>

    @Query("DELETE FROM FrostParkingSensor")
    suspend fun delete()
}