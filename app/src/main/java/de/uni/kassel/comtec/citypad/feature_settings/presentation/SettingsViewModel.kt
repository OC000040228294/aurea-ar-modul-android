package de.uni.kassel.comtec.citypad.feature_settings.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import de.uni.kassel.comtec.citypad.core.data.DataRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SettingsViewModel @Inject constructor(
    private val dataRepository: DataRepository
): ViewModel() {
    private val _soundOn = dataRepository.prefSoundOn
    val soundOn: Flow<Boolean> = _soundOn

    private val _vibrationdOn = dataRepository.prefVibrationOn
    val vibrationOn: Flow<Boolean> = _vibrationdOn

    fun setSoundOn(state: Boolean) {
        viewModelScope.launch {
            dataRepository.setPrefSoundOn(state)
        }
    }

    fun setVibrationOn(state: Boolean) {
        viewModelScope.launch {
            dataRepository.setPrefVibrationOn(state)
        }
    }
}