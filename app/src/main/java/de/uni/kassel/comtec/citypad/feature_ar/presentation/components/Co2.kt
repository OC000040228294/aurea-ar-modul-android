package de.uni.kassel.comtec.citypad.feature_ar.presentation.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import de.uni.kassel.comtec.citypad.R
import de.uni.kassel.comtec.citypad.core.data.database.model.FrostCo2Sensor
import de.uni.kassel.comtec.citypad.framework.core.data.database.model.LatLngAlt
import de.uni.kassel.comtec.citypad.core.data.network.model.FrostCo2SensorClass
import de.uni.kassel.comtec.citypad.core.domain.model.ArtefactType
import de.uni.kassel.comtec.citypad.ui.theme.CityPadTheme

@Composable
@Preview
fun DrawCo2Preview() {
    CityPadTheme {
        DrawCo2(
            FrostCo2Sensor(
                id = 9,
                name = "Luftqualität",
                latLngAlt = LatLngAlt(51.311761, 9.474192, 168.35),
                type = ArtefactType.CO2,
                co2_ppm = 420,
                co2_class = FrostCo2SensorClass.NORMAL
            ),
            2133f)
    }
}

@Composable
fun DrawCo2(
    sensor: FrostCo2Sensor, distance: Float
) {
    Box(
        modifier = sensorCardViewModifier,
        contentAlignment = Alignment.BottomCenter
    ) {
        Card(
            modifier = Modifier
                .fillMaxSize()
                .shadow(
                    elevation = 4.dp,
                    shape = RoundedCornerShape(16.dp),
                    ambientColor = MaterialTheme.colorScheme.onBackground,
                    spotColor = MaterialTheme.colorScheme.onBackground
                ),
            colors = CardDefaults.cardColors(
                containerColor = MaterialTheme.colorScheme.surface,
            ),
        ) {
            Row(
                modifier = Modifier.fillMaxWidth(),
            ) {
                DrawTitleRow(sensor.name, R.drawable.ic_co2)
            }
            Column(
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier
                    .fillMaxSize()
                    .padding(bottom = 16.dp)
            ) {
                Row(
                    horizontalArrangement = Arrangement.SpaceAround,
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Image(
                        painter = painterResource(R.drawable.ic_co2),
                        modifier = Modifier.width(50.dp),
                        contentDescription = null,
                    )
                    Spacer(Modifier.width(8.dp))
                    Text(text = sensor.co2_ppm.toString() + " ppm")
                }
                Row {
                    Text(text = sensor.co2_class.value, style = MaterialTheme.typography.headlineSmall, color = MaterialTheme.colorScheme.secondary)
                }
            }
        }
        Box(
            modifier = Modifier.offset(y = (16).dp),
        ) {
            DrawDistanceView(distance)
        }
    }
}