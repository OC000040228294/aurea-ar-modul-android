package de.uni.kassel.comtec.citypad.feature_compass.presentation

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.gestures.draggable
import androidx.compose.foundation.gestures.rememberDraggableState
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import de.uni.kassel.comtec.citypad.R
import de.uni.kassel.comtec.citypad.feature_compass.domain.model.AureaCompassItem
import de.uni.kassel.comtec.citypad.feature_compass.domain.model.CompassRing
import de.uni.kassel.comtec.citypad.feature_compass.presentation.components.CompassView
import de.uni.kassel.comtec.citypad.feature_compass.presentation.components.HintCompassItemNearby
import de.uni.kassel.comtec.citypad.feature_compass.presentation.components.HintCompassItemSelected
import de.uni.kassel.comtec.citypad.feature_compass.presentation.components.SegmentedControl

/**
 * CompassScreen is the main screen of the compass feature.
 * It contains the compass view and the segmented control.
 * The segmented control is used to switch between the different compass rings.
 * @param viewModel The [CompassViewModel] used to manage the state of the compass feature.
 * @param lockedCompassItemCallback Callback to get the locked [AureaCompassItem].
 * @param compassRingCallback Callback to get the active [CompassRing].
 */
@Composable
fun CompassScreen(
    viewModel: CompassViewModel = hiltViewModel(),
    lockedCompassItemCallback: (AureaCompassItem?) -> Unit = {},
    compassRingCallback: (CompassRing) -> Unit = {},
) {

    val lockedCompassItem by viewModel.lockedCompassItem.collectAsState()

    val activeCompassRing by viewModel.activeCompassRing.collectAsState()
    compassRingCallback(activeCompassRing)

    lockedCompassItemCallback.invoke(lockedCompassItem)

    var dragStarted = false
    // Swiping on the the compass view up or down will change the selected compass ring.
    val dragState = rememberDraggableState(
        onDelta = {
            if (dragStarted) {
                if (it < 0) {
                    viewModel.increaseSelectedCompassRing()
                    dragStarted = false
                } else if (it > 0) {
                    viewModel.decreaseSelectedCompassRing()
                    dragStarted = false
                }
            }
        }
    )

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight()
    ) {
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 16.dp, end = 16.dp)
        ) {
            Card(
                elevation = CardDefaults.cardElevation(defaultElevation = 4.dp),
                shape = RoundedCornerShape(8.dp),
                modifier = Modifier
                    .align(Alignment.CenterEnd)
                    .padding(start = 8.dp, top = 8.dp)
            ) {
                Row(
                    modifier = Modifier
                        .background(Color.White)
                ) {
                    Image(
                        painter = painterResource(id = R.drawable.logo_smartkassel),
                        contentDescription = "Logo von SmartKassel",
                        modifier = Modifier
                            .height(40.dp)
                            .padding(2.dp)
                    )
                }
            }
        }

        Column(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight()
                .draggable(
                    dragState,
                    onDragStarted = { dragStarted = true },
                    orientation = Orientation.Vertical
                )
        ) {

            CompassView()

            Spacer(modifier = Modifier.height(6.dp))

            Box(
                modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.BottomStart
            ) {
                Row(verticalAlignment = Alignment.Bottom) {
                    SegmentedControl(
                        useFixedWidth = true,
                        itemWidth = 56.dp,
                        selectedItemIndex = activeCompassRing
                    ) {
                        viewModel.setSelectedCompassRing(
                            it
                        )
                    }
                    lockedCompassItem?.let {
                        if (activeCompassRing != CompassRing.NEARBY) {
                            HintCompassItemSelected(lockedCompassItem = it)
                        } else {
                            HintCompassItemNearby(lockedCompassItem = it)
                        }
                    }
                }
            }
        }
    }
}


