package de.uni.kassel.comtec.citypad.core.data.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import de.uni.kassel.comtec.citypad.core.data.database.model.FrostTreeSensor
import kotlinx.coroutines.flow.Flow

@Dao
interface FrostTreeSensorDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(frostTreeSensor: FrostTreeSensor)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(frostTreeSensors: List<FrostTreeSensor>)

    @Query("SELECT * FROM FrostTreeSensor")
    fun getAsLiveData(): LiveData<List<FrostTreeSensor>>

    @Query("SELECT * FROM FrostTreeSensor")
    fun getAsFlow(): Flow<List<FrostTreeSensor>>

    @Query("DELETE FROM FrostTreeSensor")
    suspend fun delete()
}