package de.uni.kassel.comtec.citypad.feature_contact

import android.content.res.Configuration
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.ModalBottomSheet
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.rememberModalBottomSheetState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import de.uni.kassel.comtec.citypad.R
import de.uni.kassel.comtec.citypad.ui.theme.CityPadTheme

@OptIn(ExperimentalFoundationApi::class, ExperimentalMaterial3Api::class)
@Composable
fun ContactScreen() {
    val uriHandler = LocalUriHandler.current

    val scrollState = rememberScrollState()

    var showDeveloperInfo by rememberSaveable { mutableStateOf(false) }
    val bottomSheetState = rememberModalBottomSheetState(
        skipPartiallyExpanded = true
    )
    Scaffold { innerPadding ->
        Row(
            modifier = Modifier
                .padding(6.dp, 0.dp, 6.dp, 0.dp)
                .clip(RoundedCornerShape(16.dp, 16.dp, 0.dp, 0.dp))
                .padding(innerPadding)
                .fillMaxSize()
                .background(color = Color.White)
                .padding(16.dp, 0.dp, 16.dp, 0.dp)
                .verticalScroll(state = scrollState),
        ) {
            Column(horizontalAlignment = Alignment.Start) {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 16.dp),
                    horizontalArrangement = Arrangement.Center
                ) {
                    Image(
                        painter = painterResource(id = R.drawable.logo_smartkassel),
                        contentDescription = "Logo von SmartKassel",
                        modifier = Modifier
                            .width(200.dp)
                    )
                }

                Spacer(Modifier.height(32.dp))

                Text(
                    text = stringResource(R.string.contactscreen_questions),
                    style = MaterialTheme.typography.bodyLarge,
                    color = Color.Black
                )

                Spacer(modifier = Modifier.height(16.dp))

                Column(
                    modifier = Modifier
                        .padding(start = 32.dp)
                ) {
                    Row {
                        Icon(
                            painterResource(id = R.drawable.ic_mail),
                            contentDescription = "Mail Icon",
                            tint = Color.Black
                        )
                        Spacer(Modifier.width(8.dp))
                        Text(text = buildAnnotatedString {
                            pushStyle(
                                style = SpanStyle(
                                    color = MaterialTheme.colorScheme.secondary
                                )
                            )
                            append("smart@kassel.de")
                        },
                            modifier = Modifier.clickable {
                                uriHandler.openUri("mailto:smart@kassel.de")
                            }
                        )
                    }
                    Spacer(modifier = Modifier.height(16.dp))
                    Row {
                        Icon(
                            painterResource(id = R.drawable.ic_arrow_outward),
                            contentDescription = "Arrow Icon",
                            tint = Color.Black
                        )
                        Spacer(Modifier.width(8.dp))
                        Text(text = buildAnnotatedString {
                            pushStyle(
                                style = SpanStyle(
                                    color = MaterialTheme.colorScheme.secondary
                                )
                            )
                            append("Smart Kassel")
                        },
                            modifier = Modifier.clickable {
                                uriHandler.openUri("https://www.kassel.de/einrichtungen/aurea/index.php")
                            }
                        )
                    }
                }

                Spacer(Modifier.height(32.dp))

                Text(
                    stringResource(R.string.contactscreen_our_technologiepartner),
                    style = MaterialTheme.typography.titleLarge,
                    fontWeight = FontWeight.Bold,
                    color = Color.Black
                )

                Image(
                    painter = painterResource(id = R.drawable.logo_kvv),
                    contentDescription = "KVV",
                    Modifier
                        .width(280.dp)
                        .height(80.dp)
                        .padding(start = 32.dp)
                )

                Spacer(Modifier.height(16.dp))

                Text(
                    text = stringResource(id = R.string.contactscreen_developed_from),
                    style = MaterialTheme.typography.bodyLarge,
                    color = Color.Black
                )

                Spacer(Modifier.height(28.dp))

                Image(
                    painterResource(id = R.drawable.logo_uni_kassel_comtec),
                    contentDescription = "Universität Kassel, Fachgebiet ComTec",
                    Modifier
                        .combinedClickable(
                            indication = null,
                            interactionSource = remember { MutableInteractionSource() },
                            onClick = {},
                            onLongClick = {
                                showDeveloperInfo = true
                            }
                        )
                        .padding(start = 32.dp)
                )

                Spacer(Modifier.height(32.dp))

                Text(
                    stringResource(R.string.contactscreen_our_sponsors),
                    style = MaterialTheme.typography.titleLarge,
                    fontWeight = FontWeight.Bold,
                    color = Color.Black
                )

                Spacer(Modifier.height(32.dp))

                Image(
                    painterResource(id = R.drawable.logos_ministerien),
                    contentDescription = "Bundesministerium des Inneren, für Bau und Heimat; KFW",
                    Modifier
                        .padding(start = 20.dp)
                        .width(280.dp)
                )

            }
        }
    }


    if (showDeveloperInfo) {
        ModalBottomSheet(
            onDismissRequest = {
                showDeveloperInfo = false
            },
            sheetState = bottomSheetState,
            containerColor = MaterialTheme.colorScheme.background,
            scrimColor = Color.Transparent
        ) {
            Box(
                modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ) {
                Text(
                    "Entwickelt von \n\nLars Mathuseck \nSebastian Lange \nLudwig Chen Li.",
                    textAlign = TextAlign.Center
                )
            }
        }
    }
}

@Composable
@Preview(name = "Light Mode")
@Preview(name = "Dark Mode", uiMode = Configuration.UI_MODE_NIGHT_YES)
fun AboutPreview() {
    CityPadTheme {
        Surface {
            ContactScreen()
        }
    }
}