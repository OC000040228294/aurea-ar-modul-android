package de.uni.kassel.comtec.citypad.di

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import de.uni.kassel.comtec.citypad.R
import de.uni.kassel.comtec.citypad.framework.core.domain.util.sense.Sense
import de.uni.kassel.comtec.citypad.framework.core.domain.util.sense.SoundUtil
import de.uni.kassel.comtec.citypad.framework.core.domain.util.sense.VibrationUtil
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object SenseModule {

    @Provides
    @Singleton
    fun providesSense(soundUtil: SoundUtil, vibrationUtil: VibrationUtil): Sense {
        return Sense(soundUtil, vibrationUtil)
    }

    @Provides
    @Singleton
    fun providesSoundUtil(@ApplicationContext context: Context): SoundUtil {
        return SoundUtil(context, R.raw.locked_oneclick, R.raw.swipe)
    }

    @Provides
    @Singleton
    fun providesVibrationUtil(@ApplicationContext context: Context): VibrationUtil {
        return VibrationUtil(context)
    }


}