package de.uni.kassel.comtec.citypad.ui.theme

import android.app.Activity
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Typography
import androidx.compose.material3.darkColorScheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.platform.LocalView
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import androidx.core.view.WindowCompat
import de.uni.kassel.comtec.citypad.R

val LightColors = lightColorScheme(
    primary = CitypadDarkBlue,
    onPrimary = White,
    secondary = CitypadBlue,
    onSecondary = White,
    surface = White,
    surfaceVariant = CitypadBlue,
    surfaceTint = CitypadBlue,
    onSurface = CitypadBlack,
    onSurfaceVariant = CitypadGrey,
    background = White,
    onBackground = CitypadBlack,
    secondaryContainer = CitypadBlueGrey,
    onSecondaryContainer = CitypadBlue
)

val DarkColors = darkColorScheme(
    primary = CitypadDarkBlue,
    onPrimary = White,
    secondary = CitypadBlue,
    onSecondary = White,
    surface = BlackBlue,
    onSurface = White,
    surfaceTint = CitypadBlue,
    surfaceVariant = CitypadBlue,
    onSurfaceVariant = CitypadBlueGrey,
    background = BlackBlue,
    onBackground = White,
    secondaryContainer = CitypadBlueGrey,
    onSecondaryContainer = CitypadBlue
)

val CityPadTypography = Typography(
    TextStyle(
        fontFamily = FontFamily(Font(R.font.nunito)),
        fontWeight = FontWeight.Normal,
        fontSize = 16.sp,
        letterSpacing = 0.sp,
    ),
)

@Composable
fun CityPadTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable () -> Unit
) {
    val view = LocalView.current
    val colorScheme = when {
        darkTheme -> DarkColors
        else -> LightColors
    }
    if (!view.isInEditMode) {
        SideEffect {
            (view.context as Activity).window.statusBarColor = colorScheme.surface.toArgb()
            WindowCompat.getInsetsController(
                (view.context as Activity).window,
                view
            ).isAppearanceLightStatusBars =
                !darkTheme
        }
    }
    MaterialTheme(
        content = content,
        colorScheme = colorScheme,
        typography = CityPadTypography,
    )
}
