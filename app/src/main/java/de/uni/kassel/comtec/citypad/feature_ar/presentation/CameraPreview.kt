package de.uni.kassel.comtec.citypad.feature_ar.presentation

import androidx.camera.core.CameraSelector
import androidx.camera.core.Preview
import androidx.camera.extensions.ExtensionMode
import androidx.camera.extensions.ExtensionsManager
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.PreviewView
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.viewinterop.AndroidView
import androidx.core.content.ContextCompat
import timber.log.Timber

@Composable
fun CameraPreview() {
    val lifecycleOwner = LocalLifecycleOwner.current
    val context = LocalContext.current
    val cameraProviderFuture = remember { ProcessCameraProvider.getInstance(context) }
    val extensionFuture = remember { cameraProviderFuture.get() }
    val extensionsManagerFuture =
        remember { ExtensionsManager.getInstanceAsync(context, extensionFuture) }
    val extensionsManager = remember { extensionsManagerFuture.get() }

    AndroidView(
        factory = { ctx ->
            val previewView = PreviewView(ctx)
            val executor = ContextCompat.getMainExecutor(ctx)
            cameraProviderFuture.addListener({
                extensionsManagerFuture.addListener({

                    val cameraProvider = cameraProviderFuture.get()
                    val defaultCamSelector = CameraSelector.Builder()
                        .requireLensFacing(CameraSelector.LENS_FACING_BACK)
                        .build()

                    val cameraSelector = if (extensionsManager.isExtensionAvailable(
                            defaultCamSelector,
                            ExtensionMode.HDR
                        )
                    ) {
                        extensionsManager.getExtensionEnabledCameraSelector(
                            defaultCamSelector,
                            ExtensionMode.HDR
                        )
                    } else {
                        defaultCamSelector
                    }
                    try {
                        cameraProvider.unbindAll()

                        val preview = Preview.Builder().build().also {
                            it.setSurfaceProvider(previewView.surfaceProvider)
                        }

                        cameraProvider.unbindAll()
                        cameraProvider.bindToLifecycle(
                            lifecycleOwner,
                            cameraSelector,
                            preview
                        )

                    } catch (e: Exception) {
                        Timber.d("Use case binding failed: $e")
                    }
                }, executor)
            }, executor)
            previewView
        },
        modifier = Modifier.fillMaxSize(),
    )
}
