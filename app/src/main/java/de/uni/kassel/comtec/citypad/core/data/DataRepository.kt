package de.uni.kassel.comtec.citypad.core.data

import de.uni.kassel.comtec.citypad.core.data.database.AppDatabase
import de.uni.kassel.comtec.citypad.core.data.network.FrostApi
import de.uni.kassel.comtec.citypad.core.data.preferences.PreferenceStore
import de.uni.kassel.comtec.citypad.core.domain.util.launch
import de.uni.kassel.comtec.citypad.framework.core.data.database.model.LatLngAlt
import kotlinx.coroutines.flow.MutableStateFlow
import timber.log.Timber
 
// Relevant sensor ids for the frost API
val frostTreeApiIds = listOf(358, 359)
val frostTrafficApiIds = listOf(348)
val frostSwimmingPoolApiIds = listOf(334)
// val frostParkingApiIds = listOf(244, 245)
// use location of the parking Sensor to the right with Id 245
val frostCombinedParkingApiIds = listOf(245)
// val frostLightApiIds = (232..243).toList()
val frostLightApiIds = listOf(238)
val frostWaterlevelApiIds = listOf(345)
val frostCo2ApiIds = listOf(465, 466)

/**
 * Repository for fetching data from the frost API and storing it in the database
 * @param db: AppDatabase
 * @param frostApi: FrostApi
 * @param preferenceStore: PreferenceStore
 * @property treeSensorFlow: Flow<List<FrostTreeSensor>>
 *     Flow of all tree sensors in the database
 * @property parkingSensorFlow: Flow<List<FrostParkingSensor>>
 *     Flow of all parking sensors in the database
 * @property lightSensorFlow: Flow<List<FrostLightSensor>>
 *     Flow of all light sensors in the database
 * @property swimmingSensorFlow: Flow<List<FrostSwimmingPoolSensor>>
 *     Flow of all swimming pool sensors in the database
 * @property trafficSensorFlow: Flow<List<FrostTrafficSensor>>
 *     Flow of all traffic sensors in the database
 * @property waterlevelSensorFlow: Flow<List<FrostWaterlevelSensor>>
 *     Flow of all waterlevel sensors in the database
 * @property co2SensorFlow: Flow<List<FrostCo2Sensor>>
 *     Flow of all co2 sensors in the database
 *
 */
data class DataRepository(
    private val db: AppDatabase,
    private val frostApi: FrostApi,
    private val preferenceStore: PreferenceStore,
) {
    val onboardingDone = preferenceStore.onboadingDone
    val prefSoundOn = preferenceStore.soundOn
    val prefVibrationOn = preferenceStore.vibrationOn

    val loadingState = MutableStateFlow(false)

    val treeSensorFlow = db.frostTreeSensorDao().getAsFlow()
    val parkingSensorFlow = db.frostParkingSensorDao().getAsFlow()
    val lightSensorFlow = db.frostLightSensorDao().getAsFlow()
    val swimmingSensorFlow = db.frostSwimmingPoolSensorDao().getAsFlow()
    val trafficSensorFlow = db.frostTrafficSensorDao().getAsFlow()
    val waterlevelSensorFlow = db.frostWaterlevelSensorDao().getAsFlow()
    val co2SensorFlow = db.frostCo2SensorDao().getAsFlow()


    suspend fun setOnboardingDone(state: Boolean) {
        preferenceStore.setOnboardinDone(state)
    }

    suspend fun setPrefSoundOn(state: Boolean) {
        preferenceStore.setSoundOn(state)
    }

    suspend fun setPrefVibrationOn(state: Boolean) {
        preferenceStore.setVibrationOn(state)
    }

    private suspend fun insertTestSensors() {
        db.frostTreeSensorDao().insertAll(
            testTreeSensors
        )
        db.frostParkingSensorDao().insertAll(
            testParkingSensor
        )
        db.frostSwimmingPoolSensorDao().insertAll(
            testSwimmingPoolSensor
        )
        db.frostTrafficSensorDao().insertAll(
            testTrafficSensor
        )
        db.frostLightSensorDao().insertAll(
            testLightSensor
        )
        db.frostWaterlevelSensorDao().insertAll(
            testWaterlevelSensor
        )
    }

    suspend fun fetchFrostSensors(
        onLoading: (suspend () -> Unit)? = null,
        onSuccess: (suspend () -> Unit)? = null,
        onError: (suspend () -> Unit)? = null,
    ) {
        // Fetch test sensors for debugging
        //insertTestSensors()

        try {
            var numberOfErrors = 0
            loadingState.value = true
            onLoading?.invoke()
            fetchTreeData(onError = {
                numberOfErrors++
            })
            fetchTrafficData(onError = {
                numberOfErrors++
            })
            fetchSwimmingPoolData(onError = {
                numberOfErrors++
            })
            fetchCombinedParkingData(onError = {
                numberOfErrors++
            })
            fetchLightData(onError = {
                numberOfErrors++
            })
            fetchWaterlevelData(onError = {
                numberOfErrors++
            })
            fetchCo2Data(onError = {
                numberOfErrors++
            })

            if (numberOfErrors > 0) {
                onError?.invoke()
            }
            loadingState.value = false
            onSuccess?.invoke()
        } catch (exception: Exception) {
            loadingState.value = false
            Timber.e("error at fetching sensor data ${exception.message}")
            onError?.invoke()
        }
    }

    private suspend fun fetchTreeData(
        onError: (suspend () -> Unit)
    ) {
        frostTreeApiIds.forEachIndexed { i, apiId ->
            frostApi.getTreeAsync(apiId).launch(
                onSuccess = { resp ->
                    val latLngAlt = fetchLocation(apiId, 150.0)
                    if (latLngAlt == null) {
                        onError.invoke()
                        return@launch
                    }
                    db.frostTreeSensorDao().insert(
                        resp.toFrostTreeSensor(apiId, "Kastanie ${i+1}", latLngAlt)
                    )
                },
                onError = onError
            )
        }
    }

    private suspend fun fetchTrafficData(
        onError: (suspend () -> Unit)
    ) {
        frostTrafficApiIds.forEach { apiId ->
            frostApi.getTrafficAsync(apiId).launch(
                onSuccess = { resp ->

                    val latLngAlt = fetchLocation(apiId, 150.0) // TODO exchange when altitude is corrected in Android Location Service
                    if (latLngAlt == null) {
                        onError.invoke()
                        return@launch
                    }
                    db.frostTrafficSensorDao().insert(
                        resp.toFrostTrafficSensor(apiId, "Verkehr", latLngAlt)
                    )
                },
                onError = onError
            )
        }
    }

    private suspend fun fetchSwimmingPoolData(
        onError: (suspend () -> Unit)
    ) {
        frostSwimmingPoolApiIds.forEach { apiId ->
            frostApi.getSwimmingPoolAsync(apiId).launch(
                onSuccess = { resp ->
                    val latLngAlt = LatLngAlt(
                        51.301138,
                        9.500604,
                        150.0)
                    db.frostSwimmingPoolSensorDao().insert(
                        resp.toFrostSwimmingPoolSensor(apiId, "Auebad", latLngAlt)
                    )
                },
                onError = onError
            )
        }
    }

    private suspend fun fetchCombinedParkingData(
        onError: (suspend () -> Unit)
    ) {
        frostCombinedParkingApiIds.forEach { apiId ->
            frostApi.getCombinedParkingAsync().launch(
                onSuccess = { resp ->
                    val latLngAlt = fetchLocation(apiId, 150.0)
                    if (latLngAlt == null) {
                        onError.invoke()
                        return@launch
                    }
                    db.frostParkingSensorDao().insert(
                        resp.toFrostCombinedParkingSensor(apiId, "Parkplätze", latLngAlt)
                    )
                },
                onError = onError
            )
        }
    }

    private suspend fun fetchLightData(
        onError: (suspend () -> Unit)
    ) {
        frostLightApiIds.forEachIndexed { _, apiId ->
            frostApi.getLightAsync(apiId).launch(
                onSuccess = { resp ->
                    val latLngAlt = fetchLocation(apiId, 150.0)
                    if (latLngAlt == null) {
                        onError.invoke()
                        return@launch
                    }
                    db.frostLightSensorDao().insert(
                        resp.toFrostLightSensor(apiId, "Laterne", latLngAlt)
                    )
                }
            )
        }
    }

    private suspend fun fetchWaterlevelData(
        onError: (suspend () -> Unit)
    ) {
        frostWaterlevelApiIds.forEachIndexed { _, apiId ->
            frostApi.getWaterlevelAsync(apiId).launch(
                onSuccess = { resp ->
                    val latLngAlt = fetchLocation(apiId, 150.0)
                    if (latLngAlt == null) {
                        onError.invoke()
                        return@launch
                    }
                    db.frostWaterlevelSensorDao().insert(
                        resp.toFrostWaterlevelSensor(apiId, "Pegelstand Fulda", latLngAlt)
                    )
                },
                onError = onError
            )
        }
    }

    private suspend fun fetchCo2Data(
        onError: (suspend () -> Unit)
    ) {
        frostCo2ApiIds.forEachIndexed { i, apiId ->
            frostApi.getCo2Async(apiId).launch(
                onSuccess = { resp ->
                    // use location of the parking Sensor to the right with Id 245
                    val latLngAlt = fetchLocation(apiId, 150.0)
                    if (latLngAlt == null) {
                        onError.invoke()
                        return@launch
                    }
                    db.frostCo2SensorDao().insert(
                        resp.toFrostCo2Sensor(apiId, "Luftqualität ${i+1}", latLngAlt)
                    )
                },
                onError = onError
            )
        }
    }

    private suspend fun fetchLocation(
        apiId: Int,
        altitude: Double
    ): LatLngAlt? {
        val response = frostApi.getLocationAsync(apiId).await()
        if (response.isSuccessful) {
            val body = response.body()
            return body?.toLatLngAlt(altitude)
        }
        return null
    }

}