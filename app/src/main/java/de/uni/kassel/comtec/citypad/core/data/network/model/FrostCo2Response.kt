package de.uni.kassel.comtec.citypad.core.data.network.model

import com.squareup.moshi.Json
import de.uni.kassel.comtec.citypad.core.data.database.model.FrostCo2Sensor
import de.uni.kassel.comtec.citypad.framework.core.data.database.model.LatLngAlt
import de.uni.kassel.comtec.citypad.core.domain.model.ArtefactType

data class FrostCo2Response(
    @Json(name = "value")
    var observations: List<FrostCo2Observation>?,
) {
    fun toFrostCo2Sensor(id: Int, name: String, latLngAlt: LatLngAlt): FrostCo2Sensor {
        return FrostCo2Sensor(
            id,
            name,
            ArtefactType.CO2,
            latLngAlt,
            observations?.get(0)?.result?.values?.co2_ppm?.toInt()?: 0,
            getClass(observations?.get(0)?.result?.classify?.co2_class?: "")
        )
    }
    private fun getClass(value: String): FrostCo2SensorClass {
        return when(value) {
            "Normal" -> FrostCo2SensorClass.NORMAL
            "Noticeable" -> FrostCo2SensorClass.NOTICABLE
            "Unacceptable" -> FrostCo2SensorClass.UNACCEPTABLE
            else -> FrostCo2SensorClass.UNKNOWN
        }
    }
}

data class FrostCo2Observation(
    var result: FrostCo2ObservationResult?,
)

data class FrostCo2ObservationResult(
    var values: FrostCo2ObservationValues,
    var classify: FrostCo2ObservationClassify,
)

data class FrostCo2ObservationValues(
    @Json(name = "ppm")
    var co2_ppm: String,
)
data class FrostCo2ObservationClassify(
    @Json(name = "class")
    var co2_class: String,
)


enum class FrostCo2SensorClass(val value: String) {
    NORMAL("unbedenklich"),
    NOTICABLE("lüften"),
    UNACCEPTABLE("dringend lüften"),
    UNKNOWN("Unknown")
}