package de.uni.kassel.comtec.citypad.core.data.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import de.uni.kassel.comtec.citypad.core.data.database.model.FrostTrafficSensor
import kotlinx.coroutines.flow.Flow

@Dao
interface FrostTrafficSensorDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(frostTrafficSensor: FrostTrafficSensor)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(frostTrafficSensors: List<FrostTrafficSensor>)

    @Query("SELECT * FROM FrostTrafficSensor")
    fun getAsLiveData(): LiveData<List<FrostTrafficSensor>>

    @Query("SELECT * FROM FrostTrafficSensor")
    fun getAsFlow(): Flow<List<FrostTrafficSensor>>

    @Query("DELETE FROM FrostTrafficSensor")
    suspend fun delete()
}