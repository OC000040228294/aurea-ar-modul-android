package de.uni.kassel.comtec.citypad.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import de.uni.kassel.comtec.citypad.core.data.network.FrostApi
import de.uni.kassel.comtec.citypad.core.data.network.FrostApiBuilder
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ApiModule {

    @Provides
    @Singleton
    fun frostApi(): FrostApi {
        return FrostApiBuilder().frostApi()
    }
}