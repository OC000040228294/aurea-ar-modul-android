package de.uni.kassel.comtec.citypad.core.data.network

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import de.uni.kassel.comtec.citypad.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Builder for the KVV Frost API Client.
 */
class FrostApiBuilder {
    private val moshi: Moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()

    fun frostApi(): FrostApi =
        Retrofit.Builder()
            .baseUrl("https://iot.kvvks.de/FROST-Server/v1.1/")
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .client(initOkHttp())
            .build()
            .create(FrostApi::class.java)

    private fun initOkHttp(): OkHttpClient {
        val client = OkHttpClient.Builder()

        client.connectTimeout(15, TimeUnit.SECONDS)
        client.readTimeout(15, TimeUnit.SECONDS)

        if (BuildConfig.DEBUG) {
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BASIC
            client.addInterceptor(loggingInterceptor)
        }

        return client.build()
    }
}