package de.uni.kassel.comtec.citypad.core.data.database.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import de.uni.kassel.comtec.citypad.core.data.network.model.FrostTreeSensorMoisture
import de.uni.kassel.comtec.citypad.core.domain.model.Artefact
import de.uni.kassel.comtec.citypad.core.domain.model.ArtefactType
import de.uni.kassel.comtec.citypad.framework.core.data.database.model.LatLngAlt

@Entity
data class FrostTreeSensor(
    @PrimaryKey override val id: Int,
    override val name: String,
    @Embedded override val latLngAlt: LatLngAlt,
    override val type: ArtefactType,
    val moisture1: FrostTreeSensorMoisture,
    val moisture2: FrostTreeSensorMoisture,
    val moisture3: FrostTreeSensorMoisture,
) : Artefact()
