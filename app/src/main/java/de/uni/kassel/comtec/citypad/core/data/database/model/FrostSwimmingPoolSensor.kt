package de.uni.kassel.comtec.citypad.core.data.database.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import de.uni.kassel.comtec.citypad.core.data.network.model.FrostSwimmingPoolSensorCapacity
import de.uni.kassel.comtec.citypad.core.domain.model.Artefact
import de.uni.kassel.comtec.citypad.core.domain.model.ArtefactType
import de.uni.kassel.comtec.citypad.framework.core.data.database.model.LatLngAlt

@Entity
data class FrostSwimmingPoolSensor(
    @PrimaryKey override val id: Int,
    override val name: String,
    override val type: ArtefactType,
    @Embedded override val latLngAlt: LatLngAlt,
    val sauna: Int,
    val indoor: Int,
    val outdoor: Int,
    val capacity_sauna: FrostSwimmingPoolSensorCapacity,
    val capacity_indoor: FrostSwimmingPoolSensorCapacity,
    val capacity_outdoor: FrostSwimmingPoolSensorCapacity,
): Artefact()
