package de.uni.kassel.comtec.citypad.feature_ar.presentation.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import de.uni.kassel.comtec.citypad.R
import de.uni.kassel.comtec.citypad.core.data.database.model.FrostWaterlevelSensor

import de.uni.kassel.comtec.citypad.framework.core.data.database.model.LatLngAlt
import de.uni.kassel.comtec.citypad.core.domain.model.ArtefactType
import de.uni.kassel.comtec.citypad.ui.theme.CityPadTheme

@Composable
@Preview
fun DrawWaterlevelPreview() {
    CityPadTheme {
        DrawWaterlevel(
            FrostWaterlevelSensor(
                id = 9,
                name = "Wasserpegel",
                latLngAlt = LatLngAlt(51.311761, 9.474192, 168.35),
                type = ArtefactType.WATERLEVEL,
                waterlevel = 2000
            ),
            2133f)
    }
}
@Composable
fun DrawWaterlevel(
    sensor: FrostWaterlevelSensor, distance: Float
) {
    Box(
        modifier = sensorCardViewModifier,
        contentAlignment = Alignment.BottomCenter
    ) {
        Card(
            modifier = Modifier.fillMaxSize()
                .shadow(
                    elevation = 4.dp,
                    shape = RoundedCornerShape(16.dp),
                    ambientColor = MaterialTheme.colorScheme.onBackground,
                    spotColor = MaterialTheme.colorScheme.onBackground),
            colors = CardDefaults.cardColors(
                containerColor = MaterialTheme.colorScheme.surface,
            ),
        ) {
            Row(
                modifier = Modifier.fillMaxWidth(),
            ) {
                DrawTitleRow(sensor.name, R.drawable.ic_wasserpegel)
            }
            Column(
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier
                    .padding(30.dp)
                    .fillMaxSize()
            ) {
                Row(
                    horizontalArrangement = Arrangement.SpaceAround,
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Column(
                        verticalArrangement = Arrangement.spacedBy(10.dp),
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        Text("Wasserstand in mm:")
                        Text(
                            text = String.format("%,d", sensor.waterlevel),
                            color = MaterialTheme.colorScheme.secondary
                        )
                    }
                }
            }
        }
        Box(
            modifier = Modifier.offset(y = (16).dp),
        ) {
            DrawDistanceView(distance)
        }
    }
}