package de.uni.kassel.comtec.citypad.core.data.network.model

import com.squareup.moshi.Json
import de.uni.kassel.comtec.citypad.core.data.database.model.FrostSwimmingPoolSensor
import de.uni.kassel.comtec.citypad.framework.core.data.database.model.LatLngAlt
import de.uni.kassel.comtec.citypad.core.domain.model.ArtefactType

data class FrostSwimmingPoolResponse(
    @Json(name = "value")
    var observations: List<FrostSwimmingPoolObservation>?,
) {
    fun toFrostSwimmingPoolSensor(id: Int, name: String, latLngAlt: LatLngAlt): FrostSwimmingPoolSensor {
        return FrostSwimmingPoolSensor(
            id,
            name,
            ArtefactType.POOL,
            latLngAlt,
            observations?.get(0)?.result?.counter?.sauna?:0,
            observations?.get(0)?.result?.counter?.indoor?:0,
            observations?.get(0)?.result?.counter?.outdoor?:0,
            getCapacity(observations?.get(0)?.result?.capacity?.sauna?:""),
            getCapacity(observations?.get(0)?.result?.capacity?.indoor?:""),
            getCapacity(observations?.get(0)?.result?.capacity?.outdoor?:"")
        )
    }

    private fun getCapacity(value: String): FrostSwimmingPoolSensorCapacity {
        return when(value) {
            "Low" -> FrostSwimmingPoolSensorCapacity.LOW
            "Medium" -> FrostSwimmingPoolSensorCapacity.MEDIUM
            "High" -> FrostSwimmingPoolSensorCapacity.HIGH
            else -> FrostSwimmingPoolSensorCapacity.UNKNOWN
        }
    }
}

data class FrostSwimmingPoolObservation(
    var result: FrostSwimmingPoolObservationResult?,
)

data class FrostSwimmingPoolObservationResult(
    @Json(name = "values")
    val counter: FrostSwimmingPoolCounter?,
    @Json(name = "capacity")
    val capacity: FrostSwimmingPoolCapacity?,
)

data class FrostSwimmingPoolCounter(
    @Json(name = "sauna")
    var sauna: Int,
    @Json(name = "hallenbad")
    var indoor: Int,
    @Json(name = "freibad")
    var outdoor: Int,
)

data class FrostSwimmingPoolCapacity(
    @Json(name = "sauna")
    val sauna: String,
    @Json(name = "hallenbad")
    val indoor: String,
    @Json(name = "freibad")
    val outdoor: String
)

enum class FrostSwimmingPoolSensorCapacity(val value: String) {
    LOW("Low"),
    MEDIUM("Medium"),
    HIGH("High"),
    UNKNOWN("Unknown")
}

