package de.uni.kassel.comtec.citypad.core.presentation.navigation

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import de.uni.kassel.comtec.citypad.R

/**
 * Class defining the screens we have in the app
 */
sealed class Screen(val route: String, @StringRes val titleResId: Int, @DrawableRes val iconResId: Int) {
    object About : Screen( route = "about", titleResId= R.string.nav_about, iconResId = R.drawable.ic_info)
    object Main : Screen(route = "main", titleResId= R.string.nav_main, iconResId = R.drawable.ic_navigation)
    object Settings : Screen( route = "settings", titleResId= R.string.nav_settings, iconResId = R.drawable.ic_settings)
    object Contact : Screen( route = "contact", titleResId= R.string.nav_contact, iconResId = R.drawable.ic_contact)
}