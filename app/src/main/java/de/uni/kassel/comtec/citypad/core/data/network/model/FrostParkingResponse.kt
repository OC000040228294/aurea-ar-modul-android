package de.uni.kassel.comtec.citypad.core.data.network.model

import com.squareup.moshi.Json
import de.uni.kassel.comtec.citypad.core.data.database.model.FrostParkingSensor
import de.uni.kassel.comtec.citypad.framework.core.data.database.model.LatLngAlt
import de.uni.kassel.comtec.citypad.core.domain.model.ArtefactType

data class FrostParkingResponse(
    @Json(name = "value")
    var observations: List<FrostParkingObservation>?,
) {
    fun toFrostParkingSensor(id: Int, name: String, latLngAlt: LatLngAlt): FrostParkingSensor {
        return FrostParkingSensor(
            id,
            name,
            ArtefactType.PARKING,
            latLngAlt,
            state_left = getParkingStatus(observations?.get(0)?.result?.state?: ""),
            state_right = getParkingStatus("")
        )
    }

    private fun getParkingStatus(value: String): FrostParkingSensorState {
        return when(value) {
            "vacant" -> FrostParkingSensorState.FREE
            "occupied" -> FrostParkingSensorState.OCCUPIED
            else -> FrostParkingSensorState.UNKNOWN
        }
    }
}

data class FrostParkingObservation(
    var result: FrostParkingObservationResult?,
)

data class FrostParkingObservationResult(
    @Json(name = "p_state")
    var state: String,
)

enum class FrostParkingSensorState(val value: String) {
    FREE("vacant"),
    OCCUPIED("occupied"),
    UNKNOWN("Unknown")
}