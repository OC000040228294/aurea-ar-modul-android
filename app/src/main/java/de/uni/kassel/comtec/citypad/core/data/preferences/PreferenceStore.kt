package de.uni.kassel.comtec.citypad.core.data.preferences

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.preferencesDataStore
import de.uni.kassel.comtec.citypad.core.data.preferences.PreferencesKeys.KEY_ONBOARDING_DONE
import de.uni.kassel.comtec.citypad.core.data.preferences.PreferencesKeys.KEY_SOUND_ON
import de.uni.kassel.comtec.citypad.core.data.preferences.PreferencesKeys.KEY_VIBRATION_ON
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "settings")

/**
 * Preferences keys
 */
object PreferencesKeys {
    val KEY_ONBOARDING_DONE = booleanPreferencesKey("key_onboarding_done")
    val KEY_SOUND_ON = booleanPreferencesKey("key_sound_on")
    val KEY_VIBRATION_ON = booleanPreferencesKey("key_vibration_on")
}

/**
 * Apps preferences store
 */
class PreferenceStore(
    private val context: Context
) {
    val onboadingDone: Flow<Boolean> = context.dataStore.data
        .map { preferences ->
            preferences[KEY_ONBOARDING_DONE] ?: false
        }
    val soundOn: Flow<Boolean> = context.dataStore.data
        .map { preferences ->
            preferences[KEY_SOUND_ON] ?: true
        }
    val vibrationOn: Flow<Boolean> = context.dataStore.data
        .map { preferences ->
            preferences[KEY_VIBRATION_ON] ?: true
        }


    suspend fun setOnboardinDone(done: Boolean) {
        context.dataStore.edit {
            it[KEY_ONBOARDING_DONE] = done
        }
    }

    suspend fun setSoundOn(done: Boolean) {
        context.dataStore.edit {
            it[KEY_SOUND_ON] = done
        }
    }

    suspend fun setVibrationOn(done: Boolean) {
        context.dataStore.edit {
            it[KEY_VIBRATION_ON] = done
        }
    }
}