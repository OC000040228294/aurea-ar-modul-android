package de.uni.kassel.comtec.citypad.di

import android.content.Context
import android.hardware.SensorManager
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.SettingsClient
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import de.uni.kassel.comtec.citypad.core.data.DataRepository
import de.uni.kassel.comtec.citypad.framework.core.data.SensorRepository
import de.uni.kassel.comtec.citypad.core.data.database.AppDatabase
import de.uni.kassel.comtec.citypad.core.data.network.FrostApi
import de.uni.kassel.comtec.citypad.core.data.preferences.PreferenceStore
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {
    @Provides
    @Singleton
    fun providesAppRepository(
        database: AppDatabase,
        frostApi: FrostApi,
        prefs: PreferenceStore,
    ): DataRepository {
        return DataRepository(database, frostApi, prefs)
    }

    @Provides
    @Singleton
    fun providesSensorRepository(
        sensorManager: SensorManager,
        locationProvider: FusedLocationProviderClient,
        settingsClient: SettingsClient
    ): SensorRepository {
        return SensorRepository(sensorManager, locationProvider, settingsClient)
    }

    @Provides
    @Singleton
    fun providesPrefStore(@ApplicationContext context: Context) =
        PreferenceStore(context = context)

}