package de.uni.kassel.comtec.citypad.core.data

import de.uni.kassel.comtec.citypad.core.data.database.model.FrostLightSensor
import de.uni.kassel.comtec.citypad.core.data.database.model.FrostParkingSensor
import de.uni.kassel.comtec.citypad.core.data.database.model.FrostSwimmingPoolSensor
import de.uni.kassel.comtec.citypad.core.data.database.model.FrostTrafficSensor
import de.uni.kassel.comtec.citypad.core.data.database.model.FrostTreeSensor
import de.uni.kassel.comtec.citypad.core.data.database.model.FrostWaterlevelSensor
import de.uni.kassel.comtec.citypad.core.data.network.model.FrostParkingSensorState
import de.uni.kassel.comtec.citypad.core.data.network.model.FrostSwimmingPoolSensorCapacity
import de.uni.kassel.comtec.citypad.core.data.network.model.FrostTreeSensorMoisture
import de.uni.kassel.comtec.citypad.core.domain.model.ArtefactType
import de.uni.kassel.comtec.citypad.framework.core.data.database.model.LatLngAlt

/**
 * Test data for sensors.
 * Can be Adjusted for local testing.
 */
val testTreeSensors = listOf (
    FrostTreeSensor(
        id = 1,
        name = "Baum 1 - WA73-Wiese",
        latLngAlt = LatLngAlt(51.311770, 9.472982, 218.0),
        type = ArtefactType.TREE,
        moisture1 = FrostTreeSensorMoisture.MEDIUM,
        moisture2 = FrostTreeSensorMoisture.LOW,
        moisture3 = FrostTreeSensorMoisture.HIGH
    ),
    FrostTreeSensor(
        id = 2,
        name = "Baum 2 - WA73-Wiese",
        latLngAlt = LatLngAlt(51.311810, 9.473087, 218.0),
        type = ArtefactType.TREE,
        moisture1 = FrostTreeSensorMoisture.HIGH,
        moisture2 = FrostTreeSensorMoisture.MEDIUM,
        moisture3 = FrostTreeSensorMoisture.LOW
    ),
    FrostTreeSensor(
        id = 3,
        name = "Baum 3 - WA73-Wiese",
        latLngAlt = LatLngAlt(51.311973, 9.472986, 168.35+2),
        type = ArtefactType.TREE,
        moisture1 = FrostTreeSensorMoisture.LOW,
        moisture2 = FrostTreeSensorMoisture.MEDIUM,
        moisture3 = FrostTreeSensorMoisture.HIGH
    ),
    FrostTreeSensor(
        id = 4,
        name = "Baum 4 - WA73-Seiteneingang",
        latLngAlt = LatLngAlt(51.311143, 9.472679, 218.0),
        type = ArtefactType.TREE,
        moisture1 = FrostTreeSensorMoisture.MEDIUM,
        moisture2 = FrostTreeSensorMoisture.HIGH,
        moisture3 = FrostTreeSensorMoisture.HIGH
    )
)

val testParkingSensor = listOf(
    FrostParkingSensor(
        id = 5,
        name = "Parkplätze - WA73",
        latLngAlt = LatLngAlt(51.312057, 9.473029, 168.35),
        type = ArtefactType.PARKING,
        state_left = FrostParkingSensorState.OCCUPIED,
        state_right = FrostParkingSensorState.OCCUPIED
    ),
)

val testSwimmingPoolSensor = listOf(
    FrostSwimmingPoolSensor(
        id = 7,
        name = "FitnessFirst - WA73",
        latLngAlt = LatLngAlt(51.311791, 9.472573, 168.35+2),
        type = ArtefactType.POOL,
        sauna = 100,
        indoor = 150,
        outdoor = 55,
        capacity_sauna = FrostSwimmingPoolSensorCapacity.LOW,
        capacity_indoor = FrostSwimmingPoolSensorCapacity.MEDIUM,
        capacity_outdoor = FrostSwimmingPoolSensorCapacity.HIGH
    )
)

val testTrafficSensor = listOf(
    FrostTrafficSensor(
        id = 8,
        name = "Verkehr - WA73",
        latLngAlt = LatLngAlt(51.312188, 9.472786, 168.35),
        type = ArtefactType.TRAFFIC,
        hugeSum = 20,
        largeSum = 50,
        mediumSum = 1000,
        smallSum = 30
    )
)

val testLightSensor = listOf(
    FrostLightSensor(
        id = 9,
        name = "Laterne - WA73",
        latLngAlt = LatLngAlt(51.311761, 9.474192, 168.35),
        type = ArtefactType.LIGHT,
        led_brightness = 30
    )
)

val testWaterlevelSensor = listOf(
    FrostWaterlevelSensor(
        id = 10,
        name = "Pegelstand - WA73",
        latLngAlt = LatLngAlt(51.311761, 9.474180, 168.32),
        type = ArtefactType.WATERLEVEL,
        waterlevel = 4999
    )
)