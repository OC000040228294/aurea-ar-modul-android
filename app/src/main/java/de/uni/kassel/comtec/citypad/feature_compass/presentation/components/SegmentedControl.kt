package de.uni.kassel.comtec.citypad.feature_compass.presentation.components
// Code adapted from: https://makb.medium.com/hello-everyone-558290eb632e

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.zIndex
import de.uni.kassel.comtec.citypad.feature_compass.domain.model.CompassRing
import de.uni.kassel.comtec.citypad.ui.theme.CityPadTheme

@Composable
@Preview
fun PreviewSegmentedControl() {
    CityPadTheme() {
        Surface() {
            SegmentedControl(onItemSelection = {}, useFixedWidth = true, itemWidth = 56.dp)
        }
    }

}

/**
 * SegmentedControl is the component that is used to select the compass ring.
 * Code adapted from: https://makb.medium.com/hello-everyone-558290eb632e
 *
 * @param selectedItemIndex The currently selected compass ring.
 * @param useFixedWidth If true, the buttons will have a fixed width.
 * @param itemWidth The width of the buttons.
 * @param cornerRadius The corner radius of the buttons.
 * @param color The color of the buttons.
 * @param onItemSelection The callback that is called when a button is pressed.
 *
 */
@Composable
fun SegmentedControl(
    selectedItemIndex: CompassRing = CompassRing.NEARBY,
    useFixedWidth: Boolean = false,
    itemWidth: Dp = 120.dp,
    cornerRadius : Int = 16,
    color : Color = MaterialTheme.colorScheme.secondary,
    onItemSelection: (selectedItemIndex: CompassRing) -> Unit
) {
    Column(
        modifier = Modifier.padding(start = 16.dp).offset(0.dp, Dp(CompassRing.values().size.toFloat() * 3.5f)),
        verticalArrangement = Arrangement.Bottom
    ) {
        CompassRing.values().forEachIndexed { index, item ->
            OutlinedButton(
                modifier = when (index) {
                    0 -> {
                        if (useFixedWidth) {
                            Modifier
                                .width(itemWidth)
                                .offset(0.dp, 0.dp)
                                .zIndex(if (selectedItemIndex.ordinal == item.ordinal) 1f else 0f)
                        } else {
                            Modifier
                                .wrapContentSize()
                                .offset(0.dp, 0.dp)
                                .zIndex(if (selectedItemIndex.ordinal == item.ordinal) 1f else 0f)
                        }
                    } else -> {
                        if (useFixedWidth)
                            Modifier
                                .width(itemWidth)
                                .offset(0.dp, (-9 * index).dp)
                                .zIndex(if (selectedItemIndex.ordinal == item.ordinal) 1f else 0f)
                        else Modifier
                            .wrapContentSize()
                            .offset(0.dp, (-1 * index).dp)
                            .zIndex(if (selectedItemIndex.ordinal == item.ordinal) 1f else 0f)
                    }
                },
                contentPadding = PaddingValues(6.dp),
                onClick = {
                    onItemSelection(item)
                },
                shape = when (index) {
                    0 -> RoundedCornerShape(
                        topStartPercent = cornerRadius,
                        topEndPercent = cornerRadius,
                        bottomStartPercent = 0,
                        bottomEndPercent = 0
                    )
                    CompassRing.values().size - 1 -> RoundedCornerShape(
                        topStartPercent = 0,
                        topEndPercent = 0,
                        bottomStartPercent = cornerRadius,
                        bottomEndPercent = cornerRadius
                    )
                    else -> RoundedCornerShape(
                        topStartPercent = 0,
                        topEndPercent = 0,
                        bottomStartPercent = 0,
                        bottomEndPercent = 0
                    )
                },
                border = BorderStroke(
                    1.dp, if (selectedItemIndex.ordinal == index) {
                        color
                    } else {
                        color.copy(alpha = 0.75f)
                    }
                ),
                colors = if (selectedItemIndex.ordinal == index) {
                    ButtonDefaults.outlinedButtonColors(
                        containerColor = color
                    )
                } else {
                    ButtonDefaults.outlinedButtonColors(containerColor = Color.Transparent)
                },
            ) {
                Text(
                    text = when(index) {
                        3 -> "Nah"
                        2 -> "${CompassRing.NEAR.range.last}m"
                        1 -> "${CompassRing.MED.range.last}m"
                        0 -> "∞"
                        else -> "...m"
                    },
                    fontWeight = FontWeight.Normal,
                    color = if (selectedItemIndex.ordinal == index) {
                        Color.White
                    } else {
                        color.copy(alpha = 0.9f)
                    },
                    overflow = TextOverflow.Ellipsis,
                    maxLines = 1,
                )
            }
        }
    }
}