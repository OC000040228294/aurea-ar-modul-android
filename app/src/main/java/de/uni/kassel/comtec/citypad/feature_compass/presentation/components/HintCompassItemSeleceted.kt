package de.uni.kassel.comtec.citypad.feature_compass.presentation.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material3.Card
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.tooling.preview.PreviewParameterProvider
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import com.airbnb.lottie.compose.LottieAnimation
import com.airbnb.lottie.compose.LottieCompositionSpec
import com.airbnb.lottie.compose.animateLottieCompositionAsState
import com.airbnb.lottie.compose.rememberLottieComposition
import de.uni.kassel.comtec.citypad.R
import de.uni.kassel.comtec.citypad.core.data.testTrafficSensor
import de.uni.kassel.comtec.citypad.core.domain.model.getDrawableId
import de.uni.kassel.comtec.citypad.core.presentation.MainViewModel
import de.uni.kassel.comtec.citypad.feature_compass.domain.model.AureaCompassItem
import de.uni.kassel.comtec.citypad.feature_compass.domain.model.CompassRing
import de.uni.kassel.comtec.citypad.ui.theme.CityPadTheme
import kotlin.math.roundToInt

/**
 * HintCompassItemNearby is the hint that is shown when the user has selected the nearby compass ring.
 */
@Composable
fun HintCompassItemNearby(
    lockedCompassItem: AureaCompassItem,
    viewModel: MainViewModel = hiltViewModel()
) {
    val location by viewModel.location.collectAsState()
    var distanceToItem by remember {
        mutableStateOf(0f)
    }

    location?.let {
        if (lockedCompassItem.artefacts.isNotEmpty())
            distanceToItem = lockedCompassItem.artefacts[0].latLngAlt.distanceTo(it)
    }

    Box(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp)
            .fillMaxHeight(),
        contentAlignment = Alignment.BottomCenter
    ) {
        Box {
            Card(
                modifier = Modifier
                    .fillMaxWidth()
            ) {
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 30.dp, start = 8.dp, end = 8.dp, bottom = 8.dp),
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.SpaceAround
                ) {
                    val name: String =
                        if (lockedCompassItem.artefacts.isNotEmpty()) lockedCompassItem.artefacts[0].name else ""
                    Text(
                        modifier = Modifier.padding(top = 8.dp),
                        text = String.format(
                            stringResource(id = R.string.hint_compass_item_nearby_title),
                            name
                        ),
                        fontWeight = FontWeight.Bold,
                        fontSize = 16.sp,
                        color = MaterialTheme.colorScheme.onPrimary,
                        textAlign = TextAlign.Center
                    )

                    PhoneTiltHintAnimation()

                    Text(
                        modifier = Modifier.padding(top = 0.dp, bottom = 8.dp),
                        text = stringResource(id = R.string.hint_compass_item_nearby_description),
                        fontSize = 14.sp,
                        color = MaterialTheme.colorScheme.onPrimary,
                        textAlign = TextAlign.Center
                    )
                }
            }
            Title(lockedCompassItem, CompassRing.NEARBY.range.last.toFloat(), true)
        }
    }
}

/**
 * HintCompassItemSelected is the hint that is shown when the user has selected a compass item.
 */
@Composable
fun HintCompassItemSelected(
    lockedCompassItem: AureaCompassItem,
    viewModel: MainViewModel = hiltViewModel()
) {
    val location by viewModel.location.collectAsState()
    var distanceToItem by remember {
        mutableStateOf(0f)
    }

    location?.let {
        if (lockedCompassItem.artefacts.isNotEmpty())
            distanceToItem = lockedCompassItem.artefacts[0].latLngAlt.distanceTo(it)
    }

    Box(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp)
            .fillMaxHeight(),
        contentAlignment = Alignment.BottomCenter
    ) {
        Box {
            Card(
                modifier = Modifier
                    .fillMaxWidth()
            ) {
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 30.dp, start = 8.dp, end = 8.dp, bottom = 8.dp),
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.SpaceEvenly
                ) {
                    var name: String =
                        if (lockedCompassItem.artefacts.isNotEmpty()) lockedCompassItem.artefacts[0].name else ""
                    if (lockedCompassItem.artefacts.size > 1) name = String.format(
                        stringResource(R.string.hint_compass_item_selected_title_cluster_suffix),
                        name
                    )
                    Text(
                        modifier = Modifier.padding(top = 8.dp),
                        text = name,
                        fontWeight = FontWeight.Bold,
                        fontSize = 18.sp,
                        color = MaterialTheme.colorScheme.onPrimary,
                        textAlign = TextAlign.Center
                    )

                    PhoneTiltHintAnimation()

                    Text(
                        modifier = Modifier.padding(top = 8.dp, bottom = 8.dp),
                        text = stringResource(id = R.string.hint_compass_item_selected_description),
                        fontSize = 14.sp,
                        color = MaterialTheme.colorScheme.onPrimary
                    )
                }
            }
            Title(lockedCompassItem, distanceToItem, false)
        }
    }
}

@Composable
fun Title(lockedCompassItem: AureaCompassItem, distanceToItem: Float, isRange: Boolean) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(start = 16.dp, end = 16.dp)
            .height(40.dp)
            .offset(y = (-20).dp)
    ) {
        Row(
            modifier = Modifier
                .fillMaxSize()
                .background(MaterialTheme.colorScheme.primary)
                .padding(start = 8.dp, end = 8.dp),
            horizontalArrangement = Arrangement.SpaceEvenly,
            verticalAlignment = Alignment.CenterVertically,
        ) {
            if (lockedCompassItem.artefacts.isNotEmpty()) {
                Image(
                    painter = painterResource(id = lockedCompassItem.artefacts[0].getDrawableId()),
                    modifier = Modifier
                        .size(24.dp),
                    colorFilter = ColorFilter.tint(color = MaterialTheme.colorScheme.secondary),
                    contentDescription = "Sensor Icon",
                )
            }
            Spacer(modifier = Modifier.width(4.dp))
            Text(
                text = stringResource(R.string.sensor),
                fontWeight = FontWeight.Bold,
                style = MaterialTheme.typography.titleMedium,
                color = MaterialTheme.colorScheme.onPrimary,
                overflow = TextOverflow.Ellipsis,
                maxLines = 1,
            )
            Spacer(modifier = Modifier.width(4.dp))
            Text(
                text =
                    if (isRange) {"0-${CompassRing.NEARBY.range.last} m"}
                    else if (distanceToItem < 1000) "${distanceToItem.roundToInt()} m" else "${(String.format("%.2f", distanceToItem/1000f))} km",
                style = MaterialTheme.typography.titleMedium,
                color = MaterialTheme.colorScheme.onPrimary,
                overflow = TextOverflow.Ellipsis,
                maxLines = 1,
            )

        }
    }
}

@Composable
@Preview
fun PreviewHintCompassItemSelected(
    @PreviewParameter(CompassItemPreviewParameter::class) compassItem: AureaCompassItem
) {
    CityPadTheme {
        HintCompassItemSelected(compassItem)
    }
}


@Composable
@Preview
fun PreviewHintCompassItemNearby(
    @PreviewParameter(CompassItemPreviewParameter::class) compassItem: AureaCompassItem
) {
    CityPadTheme {
        HintCompassItemNearby(compassItem)
    }
}

class CompassItemPreviewParameter : PreviewParameterProvider<AureaCompassItem> {
    override val values = sequenceOf(
        AureaCompassItem(
            mutableListOf(
                testTrafficSensor[0]
            ),
            CompassRing.NEARBY,
        )
    )
}


@Composable
private fun PhoneTiltHintAnimation() {
    val composition by rememberLottieComposition(LottieCompositionSpec.RawRes(R.raw.phone))
    val progress by animateLottieCompositionAsState(
        composition,
        iterations = 4
    )
    Box(modifier = Modifier.padding(top = 0.dp, bottom = 0.dp)) {
        LottieAnimation(
            modifier = Modifier.size(100.dp),
            alignment = Alignment.Center,
            composition = composition,
            progress = { progress },
        )
    }
}
