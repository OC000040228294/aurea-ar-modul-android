package de.uni.kassel.comtec.citypad.core.data.network.model

import com.squareup.moshi.Json
import de.uni.kassel.comtec.citypad.framework.core.data.database.model.LatLngAlt


data class FrostLocationResponse(
    var value: List<FrostLocationValue>,
    ) {
    fun toLatLngAlt(altitude: Double): LatLngAlt? {
        return try {
            val coordinatesSorted = value[0].location.coordinates.sortedByDescending { it }
            LatLngAlt(coordinatesSorted[0], coordinatesSorted[1], altitude)
        } catch (e: Exception) {
            null
        }
    }
}

data class FrostLocationValue(
    @Json(name = "@iot.id")
    var id: Int,
    var location: FrostLocation,
)

data class FrostLocation(
    var coordinates: List<Double>,
)