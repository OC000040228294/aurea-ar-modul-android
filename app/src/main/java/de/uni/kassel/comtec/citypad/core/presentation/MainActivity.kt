package de.uni.kassel.comtec.citypad.core.presentation

import android.os.Bundle
import android.view.WindowManager
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.google.accompanist.pager.ExperimentalPagerApi
import dagger.hilt.android.AndroidEntryPoint
import de.uni.kassel.comtec.citypad.feature_app_intro.AppIntro
import de.uni.kassel.comtec.citypad.ui.theme.CityPadTheme
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking

/**
 * Main entry point of the app.
 */
@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        setContent {
            CityPadTheme {
                Content()
            }
        }
    }

    /**
     * Main content of the app.
     * The onboarding is shown if the user has not seen it yet.
     * Fetching of the data is started and stopped here.
     */
    @OptIn(ExperimentalAnimationApi::class, ExperimentalPagerApi::class)
    @Composable
    fun Content(
        viewModel: MainViewModel = hiltViewModel(),
        lifecycleOwner: LifecycleOwner = LocalLifecycleOwner.current,
    ) {
        DisposableEffect(lifecycleOwner) {
            viewModel.startFetching()
            val observer = LifecycleEventObserver { _, event ->
                if (event == Lifecycle.Event.ON_RESUME) {
                    viewModel.startFetching()
                } else if (event == Lifecycle.Event.ON_PAUSE) {
                    viewModel.stopFetching()
                }
            }
            lifecycleOwner.lifecycle.addObserver(observer)
            onDispose {
                viewModel.stopFetching()
                lifecycleOwner.lifecycle.removeObserver(observer)
            }
        }

        var initialOnBoardingDone by remember {
            mutableStateOf(true)
        }

        initialOnBoardingDone = runBlocking {
            viewModel.onboardingDone.first()
        }

        val navController = rememberNavController()

        NavHost(
            navController,
            startDestination = if (initialOnBoardingDone) "app" else "onboarding"
        ) {
            composable("app") {
                CityPad()
            }
            composable("onboarding") {
                AppIntro(onGettingStartedClick = {
                    viewModel.setOnboardingDone(true)
                    // remove intro from backstack navigation
                    navController.backQueue.clear()
                    navController.navigate("app") {
                        popUpTo("app") {
                            inclusive = true
                            saveState = true
                        }
                        launchSingleTop = true

                        restoreState = true
                    }
                })
            }
        }
    }
}
