package de.uni.kassel.comtec.citypad.feature_ar.presentation.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import de.uni.kassel.comtec.citypad.R
import de.uni.kassel.comtec.citypad.core.data.database.model.FrostLightSensor
import de.uni.kassel.comtec.citypad.framework.core.data.database.model.LatLngAlt
import de.uni.kassel.comtec.citypad.core.domain.model.ArtefactType
import de.uni.kassel.comtec.citypad.ui.theme.CityPadTheme

@Composable
@Preview
fun DrawLightPreview() {
    CityPadTheme {
        DrawLight(
            FrostLightSensor(
                id = 9,
                name = "Laterne",
                latLngAlt = LatLngAlt(51.311761, 9.474192, 168.35),
                type = ArtefactType.LIGHT,
                led_brightness = 80
            ),
            2133f)
    }
}
@Composable
fun DrawLight(
    sensor: FrostLightSensor, distance: Float
) {
    val brightnessIcon =
        if (sensor.led_brightness < 10) {
            R.drawable.ic_laterne_0_prozent_orange
        }
        else if (sensor.led_brightness < 50) {
            R.drawable.ic_laterne_10_prozent_orange
        }
        else if (sensor.led_brightness < 100) {
            R.drawable.ic_laterne_50_prozent_orange
        }
        else {
            R.drawable.ic_laterne_100_prozent_orange
        }

    Box(
        modifier = sensorCardViewModifier,
        contentAlignment = Alignment.BottomCenter
    ) {
        Card(
            modifier = Modifier
                .fillMaxSize()
                .shadow(
                    elevation = 4.dp,
                    shape = RoundedCornerShape(16.dp),
                    ambientColor = MaterialTheme.colorScheme.onBackground,
                    spotColor = MaterialTheme.colorScheme.onBackground
                ),
            colors = CardDefaults.cardColors(
                containerColor = MaterialTheme.colorScheme.surface,
            ),
        ) {
            Row(
                modifier = Modifier.fillMaxWidth(),
            ) {
                DrawTitleRow(sensor.name, R.drawable.ic_light)
            }
            Row(
                horizontalArrangement = Arrangement.Center,
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier
                    .fillMaxSize()
                    .padding(bottom = 16.dp)
            ) {
                Image(
                    painter = painterResource(brightnessIcon),
                    modifier = Modifier.width(64.dp),
                    contentDescription = "Laterne",
                )
                Spacer(modifier = Modifier.width(8.dp))
                Text(
                    text = "Helligkeit: ${sensor.led_brightness}%",
                    textAlign = TextAlign.Center,
                    overflow = TextOverflow.Ellipsis
                )

            }
        }
        Box(
            modifier = Modifier.offset(y = (16).dp),
        ) {
            DrawDistanceView(distance)
        }
    }
}