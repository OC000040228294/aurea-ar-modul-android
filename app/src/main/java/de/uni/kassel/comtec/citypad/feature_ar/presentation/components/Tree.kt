package de.uni.kassel.comtec.citypad.feature_ar.presentation.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import de.uni.kassel.comtec.citypad.R
import de.uni.kassel.comtec.citypad.core.data.database.model.FrostTreeSensor
import de.uni.kassel.comtec.citypad.framework.core.data.database.model.LatLngAlt
import de.uni.kassel.comtec.citypad.core.data.network.model.FrostTreeSensorMoisture
import de.uni.kassel.comtec.citypad.core.domain.model.ArtefactType
import de.uni.kassel.comtec.citypad.ui.theme.CityPadTheme


@Composable
@Preview
fun DrawTreePreview() {
    CityPadTheme {
        DrawTree(
            FrostTreeSensor(
                id = 1,
                name = "Baum 1 - WA73-Wiese",
                latLngAlt = LatLngAlt(51.311770, 9.472982, 218.0),
                type = ArtefactType.TREE,
                moisture1 = FrostTreeSensorMoisture.MEDIUM,
                moisture2 = FrostTreeSensorMoisture.LOW,
                moisture3 = FrostTreeSensorMoisture.HIGH
            ), 2135f
        )
    }
}

@Composable
fun DrawTree(
    sensor: FrostTreeSensor, distance: Float
) {
    Box(
        modifier = sensorCardViewModifier,
        contentAlignment = Alignment.BottomCenter
    ) {
        Card(
            modifier = Modifier.fillMaxSize()
                .shadow(
                    elevation = 2.dp,
                    shape = RoundedCornerShape(16.dp),
                    ambientColor = MaterialTheme.colorScheme.onBackground,
                    spotColor = MaterialTheme.colorScheme.onBackground),
            colors = CardDefaults.cardColors(
                containerColor = MaterialTheme.colorScheme.surface,
            ),
        ) {
            Row(
                modifier = Modifier.fillMaxWidth(),
            ) {
                DrawTitleRow(sensor.name, R.drawable.ic_baum)
            }
            Column(
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier
                    .fillMaxSize()
                    .padding(bottom = 16.dp)
            ) {
                for (i in 1..3) {
                    Row(
                        horizontalArrangement = Arrangement.SpaceEvenly,
                        verticalAlignment = Alignment.CenterVertically,
                        modifier = Modifier.padding(vertical = 4.dp)
                    ) {
                        when (i) {
                            1 -> {
                                Text("20cm")
                                Spacer(modifier = Modifier.width(6.dp))
                                DrawDrops(sensor.moisture1)
                            }
                            2 -> {
                                Text("50cm")
                                Spacer(modifier = Modifier.width(6.dp))
                                DrawDrops(sensor.moisture2)
                            }
                            3 -> {
                                Text("80cm")
                                Spacer(modifier = Modifier.width(6.dp))
                                DrawDrops(sensor.moisture3)
                            }
                        }
                    }
                }
            }
        }
        Box(
            modifier = Modifier.offset(y = (16).dp),
        ) {
            DrawDistanceView(distance)
        }
    }
}



@Composable
fun DrawDrops(moisture: FrostTreeSensorMoisture?) {
    when (moisture) {
        FrostTreeSensorMoisture.LOW -> {
            DrawDrop(intArrayOf(1, 0, 0))
        }

        FrostTreeSensorMoisture.MEDIUM -> {
            DrawDrop(intArrayOf(1, 1, 0))
        }

        FrostTreeSensorMoisture.HIGH -> {
            DrawDrop(intArrayOf(1, 1, 1))
        }
        else -> Text(text = "Unbekannt")
    }
}

@Composable
fun DrawDrop(i: IntArray) {
    i.forEach {
        when (it) {
            0 -> {
                Image(
                    painter = painterResource(R.drawable.ic_tropfen_leer),
                    modifier = Modifier.padding(horizontal = 4.dp).size(30.dp),
                    contentDescription = null,
                )
            }
            1 -> {
                Image(
                    painter = painterResource(R.drawable.ic_tropfen_voll),
                    modifier = Modifier.padding(horizontal = 4.dp).size(30.dp),
                    contentDescription = null,
                )
            }
        }
    }
}
