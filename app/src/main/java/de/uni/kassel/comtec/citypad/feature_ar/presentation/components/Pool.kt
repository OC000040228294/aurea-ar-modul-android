package de.uni.kassel.comtec.citypad.feature_ar.presentation.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import de.uni.kassel.comtec.citypad.R
import de.uni.kassel.comtec.citypad.core.data.database.model.FrostSwimmingPoolSensor
import de.uni.kassel.comtec.citypad.framework.core.data.database.model.LatLngAlt
import de.uni.kassel.comtec.citypad.core.data.network.model.FrostSwimmingPoolSensorCapacity
import de.uni.kassel.comtec.citypad.core.domain.model.ArtefactType
import de.uni.kassel.comtec.citypad.ui.theme.CityPadTheme

@Composable
@Preview
fun DrawPoolPreview() {
    CityPadTheme {
        DrawPool(
            FrostSwimmingPoolSensor(
                id = 7,
                name = "Auebad",
                latLngAlt = LatLngAlt(51.311791, 9.472573, 168.35+2),
                type = ArtefactType.POOL,
                sauna = 100,
                indoor = 150,
                outdoor = 550,
                capacity_sauna = FrostSwimmingPoolSensorCapacity.LOW,
                capacity_indoor = FrostSwimmingPoolSensorCapacity.MEDIUM,
                capacity_outdoor = FrostSwimmingPoolSensorCapacity.HIGH
            ),
            1500f
        )
    }
}

@Composable
fun DrawPool(
    sensor: FrostSwimmingPoolSensor,
    distance: Float
) {
    Box(
        modifier = sensorCardViewModifier,
        contentAlignment = Alignment.BottomCenter
    ) {
        Card(
            modifier = Modifier
                .fillMaxSize()
                .shadow(
                    elevation = 4.dp,
                    shape = RoundedCornerShape(16.dp),
                    ambientColor = MaterialTheme.colorScheme.onBackground,
                    spotColor = MaterialTheme.colorScheme.onBackground
                ),
            colors = CardDefaults.cardColors(
                containerColor = MaterialTheme.colorScheme.surface,
            ),
        ) {
            Row(
                modifier = Modifier.fillMaxWidth(),
            ) {
                DrawTitleRow(sensor.name, R.drawable.ic_hallenbad)
            }
            Column(
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier
                    .fillMaxSize()
                    .padding(bottom = 16.dp)
            ) {
                Row(
                    horizontalArrangement = Arrangement.SpaceEvenly,
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    for (i in 1..3) {
                        when (i) {
                            1 -> {
                                DrawPoolRow(sensor.indoor, sensor.capacity_indoor, R.drawable.ic_hallenbad)
                            }
                            2 -> {
                                DrawPoolRow(sensor.outdoor, sensor.capacity_outdoor, R.drawable.ic_freibad)
                            }
                            3 -> {
                                DrawPoolRow(sensor.sauna, sensor.capacity_sauna, R.drawable.ic_sauna)
                            }
                        }
                        Spacer(modifier = Modifier.width(10.dp))
                    }
                }
            }
        }
        Box(
            modifier = Modifier.offset(y = (16).dp),
        ) {
            DrawDistanceView(distance)
        }
    }
}

@Composable
fun DrawPoolRow(sum: Int, capacity: FrostSwimmingPoolSensorCapacity, ic: Int) {
    Column(
        verticalArrangement = Arrangement.spacedBy(10.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Image(
            painter = painterResource(ic),
            modifier = Modifier.size(40.dp),
            contentDescription = null,
        )
        Row {
            Image(
                painter = painterResource(id =
                    when(capacity) {
                        FrostSwimmingPoolSensorCapacity.LOW -> R.drawable.pool_occupancy_25
                        FrostSwimmingPoolSensorCapacity.MEDIUM -> R.drawable.pool_occupancy_50
                        FrostSwimmingPoolSensorCapacity.HIGH -> R.drawable.pool_occupancy_100
                        FrostSwimmingPoolSensorCapacity.UNKNOWN -> R.drawable.ic_question_mark
                    }
                ),
                modifier = Modifier.size(25.dp),
                contentDescription = null,
            )
            Text(text = sum.toString())
        }
    }
}