package de.uni.kassel.comtec.citypad.core.data.database.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import de.uni.kassel.comtec.citypad.core.data.network.model.FrostCo2SensorClass
import de.uni.kassel.comtec.citypad.core.domain.model.Artefact
import de.uni.kassel.comtec.citypad.core.domain.model.ArtefactType
import de.uni.kassel.comtec.citypad.framework.core.data.database.model.LatLngAlt

@Entity
data class FrostCo2Sensor(
    @PrimaryKey override val id: Int,
    override val name: String,
    override val type: ArtefactType,
    @Embedded override val latLngAlt: LatLngAlt,
    var co2_ppm: Int,
    var co2_class: FrostCo2SensorClass
): Artefact()
