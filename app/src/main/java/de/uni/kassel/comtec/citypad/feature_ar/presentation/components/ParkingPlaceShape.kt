package de.uni.kassel.comtec.citypad.feature_ar.presentation.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawWithCache
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.graphics.StrokeJoin
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import de.uni.kassel.comtec.citypad.ui.theme.CityPadTheme

@Composable
fun ParkingPlaceShape(strokeWidth: Float, color: Color, modifier: Modifier) {
    Box(modifier = modifier
        .drawWithCache {
            val path = Path()
            path.moveTo(0.0f, size.height)
            path.lineTo(0.0f, 0.0f)
            path.lineTo(size.width, 0.0f)
            path.lineTo(size.width, size.height)
            onDrawBehind {
                drawPath(
                    path,
                    color,
                    style = Stroke(
                        width = strokeWidth,
                        cap = StrokeCap.Butt,
                        join = StrokeJoin.Miter,
                    )
                )
            }
        }
        .fillMaxSize())
}

@Composable
@Preview
fun PreviewParkingPlaceShape() {
    CityPadTheme {
        Box(modifier = Modifier
            .width(90.dp)
            .height(120.dp).background(MaterialTheme.colorScheme.surface).padding(16.dp)) {
            ParkingPlaceShape(
                strokeWidth = 5f,
                color = MaterialTheme.colorScheme.onSurface,
                modifier = Modifier
            )
        }
    }
}