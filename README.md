# Aurea App Android

The AUREA module is a tool that was developed as part of the City of Kassel's Smart City concept 
(Smart Kassel | https://www.kassel.de/smart) in collaboration with the University of Kassel's 
Communication technology department (Universität Kassel | www.uni-kassel.de). 

The module enables citizens to experience the city in a completely new way. It is a building block that can be integrated 
into larger applications (such as city apps) or developed into a stand-alone AR application.

This software is the Open-Source part (Software 2) of the Aurea App for Android.  
It uses the copyright protected Framework (Software 1) for Android (Provided by [ComTec](https://www.comtec.eecs.uni-kassel.de/)). 
 
To get a licenced copy of the Framework please contact the following Mail Address: comtec@uni-kassel.de  

The copyright protected Framework (Software 1) may only be used if a licence has previously been granted by the University of Kassel.


## Requirements

Android Version: `>9.0`  
Framework (Software 1) for Android in Version `v1.0` (Provided by [ComTec](https://www.comtec.eecs.uni-kassel.de/)).

## Licence

This software is licensed under the [GNU LESSER GENERAL PUBLIC LICENSE V3](https://www.gnu.org/licenses/lgpl-3.0.en.html). 

## Development Setup

1. Clone this Repository
2. Copy the required `.aar` File into the Folder "app/src/main/libs/".
3. Open Android Studio
4. Wait for Android Studio finishing setup dependencies and indexing
5. Build and run the App

## Project Structure 

| Package               | Description |
| -                     | -           |
| core                  | Core Components of the Project. |
| core.data             | API and Database Implementation. |
| core.domain           | Modelfiles and Utils. |
| core.presentation     | Main Components and and Navigation. |
| di                    | Dependency Injection with Dagger-Hilt. |
| feature_about         | Files and Components of the About View. |
| feature_app_intro     | Files and Components of the App Intro. |
| feature_ar            | Files and Components of the AR-Feature. |
| feature_compass       | Files and Components of the Compass Feature. |
| feature_contact       | Files and Components of the Contact View. |
| feature_settings      | Files and Components of the Settings View. |


## Third Party Libraries

| Name                  | Description   |
| -                     | -             |
| lottie-compose | Lottie is a cross-platform library, that natively renders vector-based animations and art in realtime with minimal code. <br>https://github.com/airbnb/lottie-android |
| timber | Logging API.  <br>https://github.com/JakeWharton/timber |
| okhttp3 | HTTP client for Android.  <br>https://github.com/square/okhttp |
| retrofit2 | A type-safe HTTP client for Android and Java.  <br>https://github.com/square/retrofit |
| moshi | Moshi is a modern JSON library for Android, Java and Kotlin.  <br>https://github.com/square/moshi |
| Font Awesome Free | Font Awesome is the Internet's icon library and toolkit, used by millions of designers, developers, and content creators. <br> https://github.com/FortAwesome/Font-Awesome

## Workflow

* Reporting bugs:  
If you come across any issues while using the Aurea App, please report them by creating a new issue on the GitLab repository.

* Reporting bugs form:  
    ```
    App version:            1.3.0
    Android version:        14.0
    Device Name:            Google Pixel 7 Pro
    Description:            When I tap on abc, the App shows xyz instead of zyx.
    Steps to reproduce:  
    ```

* Providing feedback:   
If you have any feedback or suggestions for the Aurea App project, please let us know by creating a new issue or by sending an email to the project maintainer.

